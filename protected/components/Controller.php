<?php
/**
 * Controller is the customized base front controller class.
 * All front controllers in all modules extends from this base class.
 */
namespace application\components;

use yupe\components\controllers\Controller as BaseController;

/**
 * Class Controller
 * @package application\components
 *
 * @property string|array $title
 * @property string $metaDescription
 * @property string $metaKeywords
 * @property array $metaProperties
 * @property string $canonical
 */
class Controller extends BaseController
{
    public $layout;

    /**
     * Contains data for "CBreadcrumbs" widget (navigation element on a site,
     * a look "Main >> Category 1 >> Subcategory 1")
     */
    public $breadcrumbs = [];

    /**
     * Contains data for "CMenu" widget (provides view for menu on the site)
     */
    public $menu = [];

    public $canonical;

    public function init()
    {
        if (isset($_SERVER['HTTP_X_FORWARDED_PROTO'], $_SERVER['HTTP_HOST'], $_SERVER['REQUEST_URI'])) {
            $link = $_SERVER['HTTP_X_FORWARDED_PROTO'].'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
            $this->canonical = preg_replace(["/\/page\/2/", "/\/page\/3/", "/\/page\/4/", "/\/page\/5/", "/\/page\/6/", "/\/page\/7/", "/\/page\/8/", "/\/page\/9/", "/\/page\/10/", "/\/page\/11/", "/\/page\/12/", "/\/page\/13/", "/\/page\/14/", "/\/page\/15/", "/\/page\/16/", "/\/page\/17/", "/\/page\/18/", "/\/page\/19/", "/\/page\/20/", "/\/page\/21/", "/\/page\/22/", "/\/page\/23/", "/\/page\/24/", "/\/page\/25/", "/\/page\/26/", "/\/page\/27/", "/\/page\/28/", "/\/page\/29/", "/\/page\/30/", "/\/page\/31/", "/\/page\/32/", "/\/page\/33/", "/\/page\/34/", "/\/page\/35/", "/\/page\/36/", "/\/page\/37/", "/\/page\/38/", "/\/page\/39/", "/\/page\/40/", "/\/page\/41/", "/\/page\/42/", "/\/page\/43/", "/\/page\/44/", "/\/page\/45/", "/\/page\/46/", "/\/page\/47/", "/\/page\/48/", "/\/page\/49/", "/\/page\/50/"], "", $link);
        }


        return parent::init();
    }
}
