<?php

class m191120_122553_add_fields_url_category extends yupe\components\DbMigration
{
	public function safeUp()
	{
        $this->addColumn('{{store_category}}', 'url', "varchar(255) DEFAULT NULL");
        $this->addColumn('{{store_category}}', 'is_load', "boolean NOT NULL DEFAULT '1'");
	}

	public function safeDown()
	{
        $this->dropColumn('{{store_category}}', 'is_load');
        $this->dropColumn('{{store_category}}', 'url');
	}
}