<?php

class m191120_204400_add_field_old_id_product extends yupe\components\DbMigration
{
	public function safeUp()
	{
        $this->addColumn('{{store_product}}', 'old_id', "integer DEFAULT 0");
	}

	public function safeDown()
	{
        $this->dropColumn('{{store_product}}', 'old_id');
	}
}