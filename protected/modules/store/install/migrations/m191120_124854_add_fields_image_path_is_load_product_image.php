<?php

class m191120_124854_add_fields_image_path_is_load_product_image extends yupe\components\DbMigration
{
	public function safeUp()
	{
        $this->addColumn('{{store_product_image}}', 'image_path', "varchar(255) DEFAULT NULL");
        $this->addColumn('{{store_product_image}}', 'is_load', "boolean NOT NULL DEFAULT '1'");
	}

	public function safeDown()
	{
        $this->dropColumn('{{store_product_image}}', 'is_load');
        $this->dropColumn('{{store_product_image}}', 'image_path');
	}
}