<?php

class m191120_124041_add_fields_image_path_is_load_product extends yupe\components\DbMigration
{
	public function safeUp()
	{
        $this->addColumn('{{store_product}}', 'url', "varchar(255) DEFAULT NULL");
        $this->addColumn('{{store_product}}', 'is_load', "boolean NOT NULL DEFAULT '1'");
	}

	public function safeDown()
	{
        $this->dropColumn('{{store_product}}', 'is_load');
        $this->dropColumn('{{store_product}}', 'url');
	}
}