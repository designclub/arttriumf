<?php

class m191120_211627_add_field_image_path_product extends yupe\components\DbMigration
{
	public function safeUp()
	{
        $this->addColumn('{{store_product}}', 'image_path', "varchar(255) DEFAULT NULL");
	}

	public function safeDown()
	{
        $this->dropColumn('{{store_product}}', 'image_path');
	}
}