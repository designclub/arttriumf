<?php

class m191120_214309_add_is_processed_product extends yupe\components\DbMigration
{
	public function safeUp()
	{
        $this->addColumn('{{store_product}}', 'is_processed', "boolean NOT NULL DEFAULT '1'");
	}

	public function safeDown()
	{
        $this->dropColumn('{{store_product}}', 'is_processed');
	}
}