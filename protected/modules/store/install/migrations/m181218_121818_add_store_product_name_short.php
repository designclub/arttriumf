<?php

class m181218_121818_add_store_product_name_short extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{store_product}}', 'name_short', "string");
    }

    public function safeDown()
    {
        $this->dropColumn('{{store_product}}', 'name_short');
    }
}