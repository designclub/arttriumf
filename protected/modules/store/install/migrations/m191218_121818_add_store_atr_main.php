<?php

class m191218_121818_add_store_atr_main extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{store_attribute}}', 'out', "string");
    }

    public function safeDown()
    {
        $this->dropColumn('{{store_attribute}}', 'out');
    }
}