<?php

class m191120_162141_add_fields_category_old_id extends yupe\components\DbMigration
{
	public function safeUp()
	{
        $this->addColumn('{{store_category}}', 'old_id', "integer DEFAULT 0");
	}

	public function safeDown()
	{
        $this->dropColumn('{{store_category}}', 'old_id');
	}
}