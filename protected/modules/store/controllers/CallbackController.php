<?php
use yupe\components\controllers\FrontController;

/**
 * Class ProducerController
 */
class CallbackController extends FrontController
{
    public function actionIndex($id)
    {
        $product = Product::model()->findByPk($id);
        $model = new CallbackForm;

        if ($product) {
            $model->productName = $product->name;
            $model->productPrice = $product->price;
        }

        if (isset($_POST['CallbackForm'])) {
            $model->attributes = $_POST['CallbackForm'];
            if ($model->validate()) {
                Yii::app()->user->setFlash('success', 'Ваша заявка успешно отправлена');
                $this->refresh();
            }
        }
        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('index', [
                'model' => $model,
                'product' => $product,
            ]);
            Yii::app()->end();
        }

        $this->render('index', [
            'model' => $model,
            'product' => $product,
        ]);

    }

}
