<?php

class CategoryBackendController extends yupe\components\controllers\BackController
{
    
    public $russian = [ 'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я', 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я', ' ' ];

	public $latin = [ 'a', 'b', 'v', 'g', 'd', 'e', 'e', 'gh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'sch', 'y', 'y', 'y', 'e', 'yu', 'ya', 'a', 'b', 'v', 'g', 'd', 'e', 'e', 'gh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'sch', 'y', 'y', 'y', 'e', 'yu', 'ya', '-' ];
    
    public function actions()
    {
        return [
            'inline' => [
                'class'           => 'yupe\components\actions\YInLineEditAction',
                'model'           => 'StoreCategory',
                'validAttributes' => [
                    'status',
                    'slug'
                ]
            ],
            'sortable' => [
                'class' => 'yupe\components\actions\SortAction',
                'model' => 'StoreCategory',
                'attribute' => 'sort'
            ]
        ];
    }

    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin'],],
            ['allow', 'actions' => ['index'], 'roles' => ['Store.CategoryBackend.Index'],],
            ['allow', 'actions' => ['view'], 'roles' => ['Store.CategoryBackend.View'],],
            ['allow', 'actions' => ['create'], 'roles' => ['Store.CategoryBackend.Create'],],
            ['allow', 'actions' => ['update', 'inline', 'sortable'], 'roles' => ['Store.CategoryBackend.Update'],],
            ['allow', 'actions' => ['delete', 'multiaction'], 'roles' => ['Store.CategoryBackend.Delete'],],
            ['deny',],
        ];
    }

    /**
     * Отображает категорию по указанному идентификатору
     *
     * @param integer $id Идинтификатор категорию для отображения
     *
     * @return void
     */
    public function actionView($id)
    {
        $this->render('view', ['model' => $this->loadModel($id)]);
    }

    /**
     * Создает новую модель категории.
     * Если создание прошло успешно - перенаправляет на просмотр.
     *
     * @return void
     */
    public function actionCreate()
    {
        $model = new StoreCategory;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (($data = Yii::app()->getRequest()->getPost('StoreCategory')) !== null) {

            $model->setAttributes($data);

            if ($model->save()) {

                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('StoreModule.store', 'Record was created!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        ['create']
                    )
                );
            }
        }

        $this->render('create', ['model' => $model]);
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id the ID of the model to be updated
     *
     * @return void
     */
    public function actionUpdate($id)
    {
        // Указан ID новости страницы, редактируем только ее
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (($data = Yii::app()->getRequest()->getPost('StoreCategory')) !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('StoreCategory'));

            if ($model->save()) {

                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('StoreModule.store', 'Category was changed!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        [
                            'update',
                            'id' => $model->id,
                        ]
                    )
                );
            }
        }

        $this->render(
            'update',
            [
                'model' => $model,
            ]
        );
    }

    /**
     * Удаяет модель категории из базы.
     * Если удаление прошло успешно - возвращется в index
     *
     * @param integer $id идентификатор категории, который нужно удалить
     *
     * @return void
     *
     * @throws CHttpException
     */
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {

            $transaction = Yii::app()->db->beginTransaction();

            try {
                // поддерживаем удаление только из POST-запроса
                $this->loadModel($id)->delete();
                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser

                $transaction->commit();

                if (!isset($_GET['ajax'])) {
                    $this->redirect(
                        (array)Yii::app()->getRequest()->getPost('returnUrl', 'index')
                    );
                }
            } catch (Exception $e) {
                $transaction->rollback();

                Yii::log($e->__toString(), CLogger::LEVEL_ERROR);
            }

        } else {
            throw new CHttpException(
                400,
                Yii::t('StoreModule.store', 'Bad request. Please don\'t use similar requests anymore')
            );
        }
    }

    /**
     * Управление категориями.
     *
     * @return void
     */
    public function actionIndex()
    {
        $model = new StoreCategory('search');
        $model->unsetAttributes();  // clear any default values

        if (isset($_GET['StoreCategory'])) {
            $model->attributes = $_GET['StoreCategory'];
        }

        $this->render('index', ['model' => $model]);
    }
    
    /**
    *
    * save interiors
    *
    */
    public function actionSaveGalleries(){
        //Gallery::model()->deleteAll(); //to comment for product
        
        $criteria = new CDbCriteria;
        $criteria->condition = 'parent = 12';
        $interiors = SiteContent::model()->findAll($criteria);
        
        foreach ($interiors as $albom){
            $gallery = new Gallery;
            $gallery->name = $albom->pagetitle;
            $gallery->description = $albom->alias;
            $gallery->category_id = 1;
            $gallery->owner = Yii::app()->user->getId();
 
            if($gallery->save()){

                $criteria = new CDbCriteria;
                $criteria->condition = 'contentid = :albom';
                $criteria->params = [ ':albom' => $albom->id ];
                $images = SiteTmplvarContentvalues::model()->findAll($criteria);
 
                foreach($images as $image){
                    if($this->isJson($image->value) === true){
                        $images = json_decode($image->value, true);
 
                        foreach($images as $imageAlbom){
                            $imagePath = $imageAlbom['image'];
                            $imageNameAr = explode('/', $imagePath);
                            $imageNameExt = explode('.', end($imageNameAr));
                            
                            $image = new Image;
                            $image->name = end($imageNameAr);
                            $image->file = md5($imagePath) . '.' . end($imageNameExt);
                            $image->alt = end($imageNameAr);
                            $image->image_path = $imagePath;
                            $image->type = 0;
                            $image->is_load = 0;
                            
                            if($image->save()){
                                $imageGallery = new ImageToGallery;
                                $imageGallery->image_id = $image->id;
                                $imageGallery->gallery_id = $gallery->id;
                                $imageGallery->save();
                            }
                        }
                    }
                }
            }
        }
        echo 'end interior';
    }
    
    /**
    *
    * save decors
    *
    */
    public function actionSaveDecors(){
        //Gallery::model()->deleteAll(); //to comment for product
        
        $criteria = new CDbCriteria;
        $criteria->condition = 'id = 1096';
        $interiors = SiteContent::model()->findAll($criteria);
 
        foreach ($interiors as $albom){
            $gallery = new Gallery;
            $gallery->name = $albom->pagetitle;
            $gallery->description = $albom->alias;
            $gallery->category_id = 1;
            $gallery->owner = Yii::app()->user->getId();
 
            if($gallery->save()){

                $criteria = new CDbCriteria;
                $criteria->condition = 'contentid = :albom';
                $criteria->params = [ ':albom' => $albom->id ];
                $images = SiteTmplvarContentvalues::model()->findAll($criteria);
 
                foreach($images as $image){
                    if($this->isJson($image->value) === true){
                        $images = json_decode($image->value, true);
 
                        foreach($images as $imageAlbom){
                            $imagePath = $imageAlbom['image'];
                            $imageNameAr = explode('/', $imagePath);
                            $imageNameExt = explode('.', end($imageNameAr));
                            
                            $image = new Image;
                            $image->name = end($imageNameAr);
                            $image->file = md5($imagePath) . '.' . end($imageNameExt);
                            $image->alt = end($imageNameAr);
                            $image->image_path = $imagePath;
                            $image->type = 0;
                            $image->is_load = 0;
                            
                            if($image->save()){
                                $imageGallery = new ImageToGallery;
                                $imageGallery->image_id = $image->id;
                                $imageGallery->gallery_id = $gallery->id;
                                $imageGallery->save();
                            }
                        }
                    }
                }
            }
        }
        echo 'end decors';
    }
    
    /**
    *
    * save images
    *
    */
    public function actionSaveImages(){
        
        $module = Yii::app()->getModule('image');
        $path = Yii::getPathOfAlias('webroot');
        
        $criteria = new CDbCriteria;
        $criteria->condition = 'is_load = 0';
        
        $images = Image::model()->findAll($criteria);

        foreach($images as $image){
            $imageNameAr = explode('/', $image->image_path);
            $is_load = file_put_contents($path .'/uploads/' . $module->uploadPath . '/' . $image->file, file_get_contents('http://s41.dcmr.ru/assets/mgr/' . $image->image_path));
            
            if($is_load !==false){
                $image->is_load = 1;
                $image->save();
            }
            
        }
    }
    
    
    /**
    *
    * save categories products
    *
    */
    public function actionSaveCatalog(){
        
        $criteria = new CDbCriteria;
        $criteria->condition = 'parent = 11';
        $criteria->addNotInCondition('id', [21, 1096]);
        $catalogs = SiteContent::model()->findAll($criteria);
 
        foreach ($catalogs as $catalog){
            
            $category = new StoreCategory;
            $category->old_id = $catalog->id;
            $category->is_load = 0;
            $category->name = $catalog->pagetitle;
            $category->name_short = $catalog->pagetitle;
            $category->description = $catalog->description;
            $category->url = $catalog->uri;
            $category->meta_title = $catalog->longtitle;
            $category->meta_description = $catalog->description;
                
            $slug = preg_replace( '/[^\p{L}0-9]/iu', ' ', $catalog->pagetitle );
            $category->slug = str_replace( $this->russian, $this->latin, $slug);

            if ( !$category->save() ) {
                $category->slug = $category->slug . $category->id;
                $category->save();
            }
            
            //$this->saveDataCategory($category, $catalog->id);
            
            $this->saveCategoriesChild($category, $catalog->id);
           
            
        }
        echo 'saves catalog';
    }
    
    public function saveCategoriesChild(StoreCategory $categoryParent, $idCategory){
        $criteria = new CDbCriteria;
        $criteria->condition = 'parent = :category and isfolder = 1';
        $criteria->params = [ ':category' => $idCategory ];
        $categoriesChild = SiteContent::model()->findAll($criteria);
        
        if(count($categoriesChild) > 0){
            foreach ($categoriesChild as $catalog){

                $category = new StoreCategory;
                $category->parent_id = $categoryParent->id;
                $category->old_id = $catalog->id;
                $category->is_load = 0;
                $category->name = $catalog->pagetitle;
                $category->name_short = $catalog->pagetitle;
                $category->description = $catalog->description;
                $category->url = $catalog->uri;
                $category->meta_title = $catalog->longtitle;
                $category->meta_description = $catalog->description;

                $slug = preg_replace( '/[^\p{L}0-9]/iu', ' ', $catalog->pagetitle );
                $category->slug = str_replace( $this->russian, $this->latin, $slug);

                if ( !$category->save() ) {
                    $category->slug = $category->slug . $category->id;
                    $category->save();
                }

                //$this->saveDataCategory($category, $catalog->id);

                $this->saveCategoriesChild($category, $catalog->id);

            }
        }
        
    }

    /**
    *
    * save image category
    *
    */
    public function actionSaveDataCategory(){
        
        $criteria = new CDbCriteria;
        $criteria->condition = 'is_load = 0';
        
        $categories = StoreCategory::model()->findAll($criteria);
 
        $module = Yii::app()->getModule('store');
        $path = Yii::getPathOfAlias('webroot');
        
        foreach($categories as $category){
            
            $criteria = new CDbCriteria;
            $criteria->condition = 'contentid = :category';
            $criteria->params = [ ':category' => $category->old_id ];

            $imagesCategory = SiteTmplvarContentvalues::model()->findAll($criteria);
 
            foreach($imagesCategory as $imageCategory){

                if($this->isJson($imageCategory->value) === false){

                    $image = explode('/', $imageCategory->value);
                    $imageExt = explode('.', end($image));

                    if(count($image) > 1 && (end($imageExt) == 'jpg' || end($imageExt) == 'png' || end($imageExt) == 'jpeg' || end($imageExt) == 'gif')){

                        $imagePath = $imageCategory->value;
                        $category->is_load = 1;
                        $category->image =  md5($imageCategory->value) . '.' . end($imageExt);
                        
                        if($category->save())
                             $is_load = file_put_contents($path .'/uploads/' . $module->uploadPath . '/category/' . $category->image, file_get_contents('http://s41.dcmr.ru/assets/mgr/' . $imagePath));
                    }
                    elseif(count($image) > 1){
                        $category->short_description =  $imageCategory->value;
                        $category->is_load = 1;
                        $category->save();
                    }
                }
            }
        }
        
        echo 'save data categories';
    }

    /**
    * save all products
    *
    *
    */
    public function actionSaveProducts(){
        
        $criteria = new CDbCriteria;
        $criteria->condition = 'old_id is not null';
        
        $categories = StoreCategory::model()->findAll($criteria);
        
        foreach($categories as $category){
            $criteria = new CDbCriteria;
            $criteria->condition = 'parent = :category AND isfolder = 0';
            $criteria->params = [':category' => $category->old_id];
            
            $productList = SiteContent::model()->findAll($criteria);
            
            if(count($productList) >0){
 
                foreach($productList as $productSite){
                    $product = new Product;
                    $product->name = $productSite->pagetitle;
                    $product->name_short = $productSite->pagetitle;
                    $product->is_load = 0;
                    $product->is_processed = 0;
                    $product->old_id = $productSite->id;
                    $product->url = $productSite->uri;
                    $product->category_id = $category->id;

                    $slug = preg_replace( '/[^\p{L}0-9]/iu', ' ', $productSite->pagetitle );
                    $product->slug = str_replace( $this->russian, $this->latin, $slug);

                    if ( !$product->save() ) {
                        $product->slug = $product->slug . $product->id;
                        $product->save();
                    }

                    //$this->saveDataProduct($product);
                }
            }
        }
        echo 'save products end';
    }
    
    /**
    *
    * save all data for products
    *
    */
    public function actionSaveDataProducts(){

        $products = Product::model()->findAll([
            'condition' => 'is_processed = 0'
        ]);
        
        foreach($products as $product){
            
            $dataProduct = SiteTmplvarContentvalues::model()->findAll([
                'condition' => 'contentid = :id',
                'params' => [':id' => $product->old_id]
            ]);

            foreach($dataProduct as $data){

                //Основное изображение
                if($data->tmplvarid == 4){

                    $image = explode('/', $data->value);
                    $imageExt = explode('.', end($image));
                    
                    $product->image = md5($data->value) . '.' . end($imageExt);
                    $product->image_path = $data->value;
                }

                //Фотографии
                if($data->tmplvarid == 10 || $data->tmplvarid == 13){

                    if($this->isJson($data->value) === true){

                        $imagesContent = json_decode($data->value, true);

                        foreach($imagesContent as $imageProduct){

                            $imagePath = $imageProduct['image'];
                            $imageNameAr = explode('/', $imagePath);
                            $imageNameExt = explode('.', end($imageNameAr));

                            $productImage = new ProductImage;
                            $productImage->product_id = $product->id;
                            $productImage->image_path = $imagePath;
                            $productImage->is_load = 0;
                            $productImage->name = md5($imagePath) . '.' . end($imageNameExt);
                            $productImage->save();
                        }
                    }

                }

                //Характеристики
                if($data->tmplvarid == 11){
                    $product->description = $data->value;
                }

                //Цена
                if($data->tmplvarid == 12){
                    $product->price = str_replace(' ', '', $data->value);
                }

                if($product->save())
                    $product->is_processed = 1;
 
            }
        }
        
        echo 'save products data end';
    }
    
    /**
    *
    * save images product
    *
    */
    public function actionSaveImagesProduct(){
        
        $criteria = new CDbCriteria;
        $criteria->condition = 'is_load = 0';
        
        $products = Product::model()->findAll($criteria);

        $module = Yii::app()->getModule('store');
        $path = Yii::getPathOfAlias('webroot');
        
        foreach($products as $product){
            if(!empty($product->image_path)){
                
                $is_load = file_put_contents($path .'/uploads/' . $module->uploadPath . '/product/' . $product->image, file_get_contents('http://s41.dcmr.ru/assets/mgr/' . $product->image_path));

                if($is_load !==false){
                    $product->is_load = 1;
                    $product->save();
                }
            }
        }
        
        echo 'save images product';
    }
    
    /**
    *
    * save additions product images
    *
    */
    public function actionSaveAdditionsProductImages(){
        $i = 0;
        $criteria = new CDbCriteria;
        $criteria->condition = 'is_load = 0';
        
        $products = ProductImage::model()->findAll($criteria);

        $module = Yii::app()->getModule('store');
        $path = Yii::getPathOfAlias('webroot');
echo '<pre>';
print_r(count($products));
Yii::app()->end();
        
        foreach($products as $product){
            if(!empty($product->image_path)){

                
                $pathImage = str_replace(' ', '-', $product->image_path);
                
                //$pathImageSpace = str_replace(' ', '%20', $product->image_path);
                
                if(@file_get_contents('http://s41.dcmr.ru/assets/mgr/' . $pathImage)){
                    $is_load = file_put_contents($path .'/uploads/' . $module->uploadPath . '/product/' . $product->name, file_get_contents('http://s41.dcmr.ru/assets/mgr/' . $pathImage));
                }
                /*else if(file_exists('http://s41.dcmr.ru/assets/mgr/' . $pathImageSpace)){

                    $is_load = file_put_contents($path .'/uploads/' . $module->uploadPath . '/product/' . $product->name, file_get_contents('http://s41.dcmr.ru/assets/mgr/' . $pathImageSpace));
                }*/
                else if(@file_get_contents('http://s41.dcmr.ru/assets/mgr/' . $product->image_path)){
                    $is_load = file_put_contents($path .'/uploads/' . $module->uploadPath . '/product/' . $product->name, file_get_contents('http://s41.dcmr.ru/assets/mgr/' . $product->image_path));
                }
                /*else if(file_exists('http://s41.dcmr.ru/assets/mgr/' . urlencode($product->image_path))){
                    $is_load = file_put_contents($path .'/uploads/' . $module->uploadPath . '/product/' . $product->name, file_get_contents('http://s41.dcmr.ru/assets/mgr/' . urlencode($product->image_path)));
                }*/
                else{
                    $is_load = false;
                }
                
                if($is_load !==false){
                    $product->is_load = 1;
                    $product->save();
                }
                else{
                    
echo '<pre>';
echo 'it I = ' . ++$i . '<br>';
echo 'image path product = http://s41.dcmr.ru/assets/mgr/' . $product->image_path . '<br>';
echo 'image modern path product = http://s41.dcmr.ru/assets/mgr/' . $pathImage . '<br>';
echo 'image name = '. $product->name . '<br>';
echo 'image name = '. $path .'/uploads/' . $module->uploadPath . '/product/' . $product->name . '<br>';
echo 'image product path with - = ';
var_dump(@file_get_contents('http://s41.dcmr.ru/assets/mgr/' . $pathImage));
/*                    echo '<br>';
var_dump(@file_get_contents('http://s41.dcmr.ru/assets/mgr/JARDIN-SECRET-J2/j2-002.jpg'));
                    
                    file_put_contents($path .'/uploads/' . $module->uploadPath . '/product/' . $product->name, file_get_contents('http://s41.dcmr.ru/assets/mgr/JARDIN-SECRET-J11/j11-002-2.jpg'));
Yii::app()->end();*/
                    
                }
            }
        }
        
        echo 'saved additions product images';
    }
    
    /**
     * Возвращает модель по указанному идентификатору
     * Если модель не будет найдена - возникнет HTTP-исключение.
     *
     * @param integer $id идентификатор нужной модели
     *
     * @return StoreCategory $model
     *
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = StoreCategory::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, Yii::t('StoreModule.store', 'Page not found!'));
        }
        return $model;
    }


    public function isJson($string) {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }


    /**
     * Производит AJAX-валидацию
     *
     * @param StoreCategory $model модель, которую необходимо валидировать
     *
     * @return void
     */
    protected function performAjaxValidation(StoreCategory $model)
    {
        if (Yii::app()->getRequest()->getIsAjaxRequest() && Yii::app()->getRequest()->getPost('ajax') === 'category-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
