<?php

/**
 * Class CheckboxFilterWidget
 */
class CheckboxFilterWidget extends \yupe\widgets\YWidget
{
    /**
     * @var string
     */
    public $view = 'checkbox-filter';

    /**
     * @var
     */
    public $attribute;

    /**
     * @throws Exception
     */
    public function init()
    {
        if (is_string($this->attribute)) {
            $this->attribute = Attribute::model()->findByAttributes(['name' => $this->attribute]);
        }



        parent::init();
    }

    /**
     * @throws CException
     */
    public function run()
    {
        $this->render($this->view, ['attribute' => $this->attribute]);
    }
}
