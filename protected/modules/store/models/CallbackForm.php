<?php

/**
 * CallbackForm
 */
class CallbackForm extends CFormModel
{
    public $name;
    public $phone;
    public $productName;
    public $productPrice;

    public function rules()
    {
        return [
            ['name, phone, productName, productPrice', 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Ваше имя',
            'phone' => 'Ваш телефон',
        ];
    }

    public function afterValidate()
    {
        if (!$this->hasErrors()) {
            Yii::app()->mailMessage->raiseMailEvent('product-order', $this->getAttributes());

        }

        return parent::afterValidate();
    }
}




