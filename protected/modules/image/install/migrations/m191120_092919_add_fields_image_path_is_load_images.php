<?php

class m191120_092919_add_fields_image_path_is_load_images extends yupe\components\DbMigration
{
	public function safeUp()
	{
        $this->addColumn('{{image_image}}', 'image_path', "varchar(255) DEFAULT NULL");
        $this->addColumn('{{image_image}}', 'is_load', "boolean NOT NULL DEFAULT '1'");
	}

	public function safeDown()
	{
        $this->dropColumn('{{image_image}}', 'is_load');
        $this->dropColumn('{{image_image}}', 'image_path');
	}
}