<?php
Yii::import('application.modules.page.forms.CalculatorForm');
/**
 * CalculatorWidget
 */
class CalculatorWidget extends \yupe\widgets\YWidget
{
    public $view = 'calculator-widget';

    public function run()
    {
        $model = new CalculatorForm;
        if (isset($_POST['CalculatorForm'])) {
            $model->attributes = $_POST['CalculatorForm'];
            if ($model->validate()) {
                Yii::app()->user->setFlash('calculator-success', 'Расчет завершен');
            }
        }

        $this->render($this->view, [
            'model' => $model,
        ]);
    }
}
