<?php

class m301204_051948_add_auto_to_page_table extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn("{{page_page}}", 'leng', 'string');
        $this->addColumn("{{page_page}}", 'tank', 'string');
        $this->addColumn("{{page_page}}", 'heating', 'string');
        $this->addColumn("{{page_page}}", 'rents', 'string');
    }

    public function safeDown()
    {
        $this->dropColumn("{{page_page}}", 'leng');
        $this->dropColumn("{{page_page}}", 'tank');
        $this->dropColumn("{{page_page}}", 'heating');
        $this->dropColumn("{{page_page}}", 'rents');
    }
}
