<?php

class m291204_051948_add_new_column_to_page_table extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn("{{page_page}}", 'new', 'string');
    }

    public function safeDown()
    {
        $this->dropColumn("{{page_page}}", 'new');
    }
}
