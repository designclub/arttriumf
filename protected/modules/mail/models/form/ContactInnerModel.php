<?php

/**
 *
 */
class ContactInnerModel extends CFormModel
{
    public $name;
    public $phone;
    public $code;
    public $verifyCode;



    public function rules()
    {
        return [
            ['name, phone', 'required'],
            ['code', 'safe'],
            ['verifyCode', 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name'  => 'Ваше имя',
            'phone' => 'Ваш телефон',
        ];
    }

     public function afterValidate()
    {
        if (empty($this->getErrors())) {
            Yii::app()->mailMessage->raiseMailEvent('zadat-vopros', $this->getAttributes());
        }
        return parent::afterValidate();
    }
}

