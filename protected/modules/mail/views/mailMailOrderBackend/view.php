<?php
/**
 * Отображение для view:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     https://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('MailModule.mail', 'Списки заявок') => ['/mail/mailMailOrderBackend/index'],
    $model->name,
];

$this->pageTitle = Yii::t('MailModule.mail', 'Списки заявок - просмотр');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('MailModule.mail', 'Управление Списками заявок'), 'url' => ['/mail/mailMailOrderBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('MailModule.mail', 'Добавить Список заявок'), 'url' => ['/mail/mailMailOrderBackend/create']],
    ['label' => Yii::t('MailModule.mail', 'Список заявок') . ' «' . mb_substr($model->id, 0, 32) . '»'],
    ['icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('MailModule.mail', 'Редактирование Списка заявок'), 'url' => [
        '/mail/mailMailOrderBackend/update',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('MailModule.mail', 'Просмотреть Список заявок'), 'url' => [
        '/mail/mailMailOrderBackend/view',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('MailModule.mail', 'Удалить Список заявок'), 'url' => '#', 'linkOptions' => [
        'submit' => ['/mail/mailMailOrderBackend/delete', 'id' => $model->id],
        'confirm' => Yii::t('MailModule.mail', 'Вы уверены, что хотите удалить Список заявок?'),
        'csrf' => true,
    ]],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('MailModule.mail', 'Просмотр') . ' ' . Yii::t('MailModule.mail', 'Списка заявок'); ?>        <br/>
        <small>&laquo;<?=  $model->name; ?>&raquo;</small>
    </h1>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView', [
    'data'       => $model,
    'attributes' => [
        'id',
        'creation_date',
        'name',
        'text:html',
    ],
]); ?>
