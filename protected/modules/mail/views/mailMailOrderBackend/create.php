<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     https://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('MailModule.mail', 'Списки заявок') => ['/mail/mailMailOrderBackend/index'],
    Yii::t('MailModule.mail', 'Добавление'),
];

$this->pageTitle = Yii::t('MailModule.mail', 'Списки заявок - добавление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('MailModule.mail', 'Управление Списками заявок'), 'url' => ['/mail/mailMailOrderBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('MailModule.mail', 'Добавить Список заявок'), 'url' => ['/mail/mailMailOrderBackend/create']],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('MailModule.mail', 'Списки заявок'); ?>
        <small><?=  Yii::t('MailModule.mail', 'добавление'); ?></small>
    </h1>
</div>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>