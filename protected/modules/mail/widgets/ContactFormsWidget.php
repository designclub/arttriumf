<?php
Yii::import('application.modules.mail.models.*');
Yii::import('application.modules.mail.models.form.ContactFormsModel');
/**
 *
 */
class ContactFormsWidget extends \yupe\widgets\YWidget
{

    public function run()
    {
        $model = new ContactFormsModel;
        if (isset($_POST['ContactFormsModel'])) {
            $model->attributes = $_POST['ContactFormsModel'];
            $mod = new MailMailOrder;
            $mod->name = "Вопрос с сайта";
            $mod->text = Yii::app()->controller->renderPartial("//mail/mail/_email-order", ['model' => $model], true);
            if ($model->validate()) {
                 $mod->save(false);
                Yii::app()->user->setFlash('success', 'Ваше сообщение успешно отправлено');
                Yii::app()->controller->refresh();
            }
        }

        $this->render('contact-form-widget', [
            'model' => $model,
        ]);
    }
}
