<?php
Yii::import('application.modules.mail.models.*');
Yii::import('application.modules.mail.models.form.CallbackFormEmailModal');

class CallbackFormEmailWidget extends yupe\widgets\YWidget
{
    public $view = 'callback-widget';


    public function run()
    {
        $model = new CallbackFormEmailModal;
        if (isset($_POST['CallbackFormEmailModal'])) {
            $model->attributes = $_POST['CallbackFormEmailModal'];
            $mod = new MailMailOrder;
            $mod->name = "Заявка на звонок";
            $mod->text = Yii::app()->controller->renderPartial("//mail/mail/_email-order", ['model' => $model], true);
            if ($model->verify == '') {
                if ($model->validate()) {
                    $mod->save(false);
                    Yii::app()->user->setFlash('success', 'Ваше сообщение успешно отправлено');
                    // Yii::app()->controller->refresh();
                }
            }
        }
        $this->render($this->view, [
            'model' => $model,
            // 'sucssesId' => $sucssesId,
        ]);
    }
}
