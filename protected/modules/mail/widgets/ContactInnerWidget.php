<?php
Yii::import('application.modules.mail.models.*');
Yii::import('application.modules.mail.models.form.ContactInnerModel');
/**
 *
 */
class ContactInnerWidget extends \yupe\widgets\YWidget
{

    public function run()
    {
        $model = new ContactInnerModel;
        if (isset($_POST['ContactInnerModel'])) {
            $model->attributes = $_POST['ContactInnerModel'];
             $mod = new MailMailOrder;
            $mod->name = "Сообщение  сайта";
            $mod->text = Yii::app()->controller->renderPartial("//mail/mail/_email-order", ['model' => $model], true);
            if ($model->validate()) {
                 $mod->save(false);
                Yii::app()->user->setFlash('success', 'Ваше сообщение успешно отправлено');
                Yii::app()->controller->refresh();
            }
        }

        $this->render('contact-form-widget', [
            'model' => $model,
        ]);
    }
}
