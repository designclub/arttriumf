<?php

/**
 *
 * Mail install migration
 * Класс миграций для модуля Mail
 *
 * @category YupeMigration
 * @package  yupe.modules.install.migrations
 * @author   YupeTeam <team@yupe.ru>
 * @license  BSD https://raw.github.com/yupe/yupe/master/LICENSE
 * @link     http://yupe.ru
 **/
class m000000_000001_mail_add_table_order extends yupe\components\DbMigration
{

    public function safeUp()
    {
        /**
         * mail_event:
         **/
        $this->createTable(
            '{{mail_mail_order}}',
            [
                'id'            => 'pk',
                'creation_date' => 'datetime NOT NULL',
                'name'          => 'varchar(150) NOT NULL',
                'text'          => 'text',
            ],
            $this->getOptions()
        );
    }

    public function safeDown()
    {
        $this->dropTableWithForeignKeys('{{mail_mail_order}}');
    }
}
