<?php
return [
    'Favorite' => 'Избранные товары',
    'Store' => 'Магазин',
    'Favorite products module' => 'Модуль для добавления товаров в избранное',
    'Success added!' => 'Добавлено в Избранное!',
    'Error =(' => 'Произошла ошибка =(',
    'Success removed!' => 'Удалено из Избранного!',
    'Remove error =(' => 'При удалении произошла ошибка =('
];