<?php
use yupe\helpers\YText;

/**
 * Class XmlCommand
 */
class XmlCommand extends \yupe\components\ConsoleCommand
{
    public function actionCategory()
    {
        $path = Yii::getPathOfAlias('webroot').'/../parserXML/product.xml';

        if (file_exists($path)) {
            $xml = simplexml_load_file($path);
            foreach ($xml->xpath("//categories/category") as $category) {
                $attributes = $category->attributes();
                $id       = null;
                $parentId = null;
                $image    = null;
                $slug     = null;
                if (isset($attributes['id'])) {
                    $id = (string)$attributes['id'];
                }
                if (isset($attributes['parentId'])) {
                    $parentId = (string)$attributes['parentId'];
                }
                if ($category->picture and isset($category->picture[0])) {
                    $image = $this->saveImage((string)$category->picture[0], 'category');
                }
                $name = trim((string)$category);
                $slug = YText::translit($name).'-'.$id;

                Yii::app()
                    ->db
                    ->createCommand()
                    ->insert('{{store_category}}', [
                        'id' => $id,
                        'name_short' => $name,
                        'name' => $name,
                        'parent_id' => $parentId,
                        'slug' => $slug,
                        'image' => $image,
                    ]);

                echo $name."\n";
            }
        }
    }

    public function actionProduct()
    {
        $path = Yii::getPathOfAlias('webroot').'/../parserXML/product.xml';

        if (file_exists($path)) {
            $xml = simplexml_load_file($path);
            foreach ($xml->xpath("//offers/offer") as $product) {
                $prod = [];
                $attributes  = $product->attributes();
                $id          = null;
                $image       = null;
                $description = '';
                if (isset($attributes['id'])) {
                    $id = (string)$attributes['id'];
                }

                if ($product->picture) {
                    $image = $this->saveImage((string)$product->picture, 'product');
                }

                $prod['image']       = $image;
                $prod['price']       = (string)$product->price;
                $prod['category_id'] = (string)$product->categoryId;
                $prod['name']        = (string)$product->name;
                $prod['name_short']  = $prod['name'];
                $prod['slug']        = YText::translit($prod['name']).'-'.$id;
                $prod['create_time'] = date('Y-m-d H:i:s');
                $prod['update_time'] = date('Y-m-d H:i:s');

                $description .= '<div class="pht-row"><strong class="pht-name">Производитель: </strong>'.((string)$product->vendor).'</div>';
                $description .= '<div class="pht-row"><strong class="pht-name">Модель: </strong>'.((string)$product->model).'</div>';

                foreach ($product->param as $key => $param) {
                    $paramAttr = $param->attributes();
                    if (isset($paramAttr['name'])) {
                        $description .= '<div class="pht-row"><strong class="pht-name">'.((string)$paramAttr['name']).': </strong>'.(string)$param.'</div>';
                    }
                }
                $prod['description'] = $description;

                try {
                    Yii::app()
                        ->db
                        ->createCommand()
                        ->insert('{{store_product}}', $prod);
                } catch (CDbException $e) {
                    echo "************* Дубль ***************\n";
                }

                echo $prod['name']."\n";
            }
        }
    }

    public function saveImage($url, $folder)
    {
        if ($url===null) {
            return null;
        }

        $path = Yii::getPathOfAlias('webroot.uploads.store').'/'.$folder;
        $ext = pathinfo($url, PATHINFO_EXTENSION);
        $name = uniqid().'.'.$ext;
        $path = $path.'/'.$name;
        file_put_contents($path, file_get_contents($url));

        return basename($path);
    }
}
