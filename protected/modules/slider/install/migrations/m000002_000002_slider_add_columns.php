<?php

class m000002_000002_slider_add_columns extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{slider}}', 'button_name2', 'varchar(250)');
        $this->addColumn('{{slider}}', 'button_link2', 'varchar(250)');
    }

    public function safeDown()
    {
        $this->dropColumn('{{slider}}', 'button_name2');
        $this->dropColumn('{{slider}}', 'button_link2');
    }
}