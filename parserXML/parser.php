<?php  
require_once 'connect_db.php';

$xml_file = "product.xml";
if (file_exists($xml_file)) {
  $xml = simplexml_load_file($xml_file);
  $i=0;

  // foreach ($xml->xpath("//categories/category") as $key => $segment) {

  //   // echo '<pre>';
  //   // print_r("$segment");
  //   // die;
  //   $id = $row["id"];
  //   $parentId = $row["parentId"];
  //   $slug = "$segment";
  //   $name = "$segment";
  //   $name_short = "$segment";
  //   $row = $segment->attributes();

  //   $ins = "insert into yupe_store_category (id, parent_id, slug, name, name_short) values(:id, :parentId, :slug, :name, :name_short)";
  //   $params = [
  //     ':id' => $id,
  //     ':parentId' => $parentId,
  //     ':slug' => $slug,
  //     ':name' => $name,
  //     ':name_short' => $name_short
  //   ];
  //   $stm = $pdo->prepare($ins);

  //   $stm->execute($params);
  // }


  foreach ($xml->xpath("//offers/offer") as $segment) {

    $id = $row["id"];
    $categoryId = $segment->categoryId;
    $name = "$segment->name";
    // $slug = "$segment->model";
    $price = $segment->price;
    $name_short = "$segment->name";
    $row = $segment->attributes();
    $ins = "insert into yupe_store_product (id, category_id, name, slug, price, name_short) values(:id, :categoryId, :name, :slug, :price, :name_short);";
    $stm = $pdo->prepare($ins);
    $params = [
    ':id' => $id,
    ':categoryId' => $categoryId,
    ':name' => $name,
    ':slug' => $slug,
    ':price' => $price,
    ':name_short' => $name_short];
    $stm->execute($params);
  }

} else {
  exit('Не удалось открыть файл '.$xml_file);
}

 ?>
