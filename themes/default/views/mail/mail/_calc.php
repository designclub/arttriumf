<table style="width: 100%;">
    <tbody>
        <tr style="background: #ececec;">
            <td style="padding: 7px 10px;"><strong><?= $model->getAttributeLabel('name')?></strong></td>
            <td style="padding: 7px 10px;"><?= $model->name; ?></td>
        </tr>
        <tr>
            <td style="padding: 7px 10px;"><strong><?= $model->getAttributeLabel('phone')?></strong></td>
            <td style="padding: 7px 10px;"><?= $model->phone; ?></td>
        </tr>
        <tr style="background: #ececec;">
            <td style="padding: 7px 10px;"><strong><?= $model->getAttributeLabel('email')?></strong></td>
            <td style="padding: 7px 10px;"><?= $model->email; ?></td>
        </tr>
        <tr>
            <td style="padding: 7px 10px;"><strong><?= $model->getAttributeLabel('material')?></strong></td>
            <td style="padding: 7px 10px;"><?= $model->material; ?></td>
        </tr>
        <tr style="background: #ececec;">
            <td style="padding: 7px 10px;"><strong><?= $model->getAttributeLabel('kview')?></strong></td>
            <td style="padding: 7px 10px;"><?= $model->kview; ?></td>
        </tr>
        <tr style="background: #ececec;">
            <td style="padding: 7px 10px;"><strong><?= $model->getAttributeLabel('height')?></strong></td>
            <td style="padding: 7px 10px;"><?= $model->height; ?></td>
        </tr>
         <tr style="background: #ececec;">
            <td style="padding: 7px 10px;"><strong><?= $model->getAttributeLabel('technic')?></strong></td>
            <td style="padding: 7px 10px;"><?= $model->technic; ?></td>
        </tr>
        <tr style="background: #ececec;">
            <td style="padding: 7px 10px;"><strong><?= $model->getAttributeLabel('txt')?></strong></td>
            <td style="padding: 7px 10px;"><?= $model->txt; ?></td>
        </tr>
    </tbody>
</table>