<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
                        'id'=>'form-inner',
                        'type' => 'vertical',
                        'htmlOptions' => ['class' => 'form', 'data-type' => 'ajax-form'],
                    ]); ?>

<div class="form-flex" data-aos="fade-up" data-aos-delay="300" data-aos-duration="3000">
    <?php if (($_SERVER['REQUEST_URI'] =='/')): ?>
        <h2 class="title_on_page">Оставьте заявку<br>
            <span class="link">на бесплатный замер</span>
        </h2>
       <div class="spec">
         Наши специалисты cвяжутся с Вами в самое ближайшее время!
       </div>
       <?php else: ?>
        <h2 class="title_on_page">Оставьте заявку<br>
            <span class="link red">и мы рассчитаем стоимость</span>
        </h2>
       <div class="spec">
         Наши специалисты cвяжутся с Вами в самое ближайшее время!
       </div>
    <?php endif ?>
    <div class="inputs-wrap">
        <div class="col-form">
        <?= $form->textFieldGroup($model, 'name', [
            'widgetOptions'=>[
                'htmlOptions'=>[
                    'class' => '',
                    'autocomplete' => 'off'
                ]
            ]
        ]); ?>
         </div>
         <div class="col-form">
           <div class="form-group">
                            <?= $form->labelEx($model, 'phone', ['class' => 'control-label']) ?>
                            <?php $this->widget('CMaskedTextFieldPhone', [
                                'model' => $model,
                                'attribute' => 'phone',
                                'mask' => '+7(999)999-99-99',
                                'htmlOptions'=>[
                                    'class' => 'data-mask form-control',
                                    'data-mask' => 'phone',
                                    'placeholder' => '  +7 (___) - ___ - __ - __',
                                    'autocomplete' => 'off'
                                ]
                            ]) ?>
                        </div>
         </div>
           <?= $form->hiddenField($model, 'code') ?>
       </div>


    <div class="sub_btn">
       <button id="form-button" class="banner_calc" data-send="ajax">Отправить заявку</button>
    </div>

   </div>
<?php if (Yii::app()->user->hasFlash('success')): ?>
    <div id="messageModal" class="modal fade in" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Закрыть">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Уведомление!</h4>
                </div>
                <div class="modal-body">
                    <?= Yii::app()->user->getFlash('success') ?>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('#messageModal').modal('show');
        setTimeout(function(){
            $('#messageModal').modal('hide');
        }, 5000);
    </script>
<?php endif ?>




<?php $this->endWidget() ?>


