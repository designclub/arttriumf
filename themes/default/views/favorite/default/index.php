<?php
$mainAssets = Yii::app()->getTheme()->getAssetsUrl();
Yii::app()->getClientScript()->registerScriptFile(
    Yii::app()->getAssetManager()->publish(
        Yii::getPathOfAlias('application.modules.favorite.view.web') . '/favorite.js'
    ),
    CClientScript::POS_END
);

$this->title = 'Избранные товары';
$this->breadcrumbs = [
    Yii::t("StoreModule.store", "Catalog") => ['/store/product/index'],
    'Избранные товары'
];
?>
<div class="container">
<div class="category-box__content">

                <?php
$this->widget(
    // 'application.components.MyClistView',
    'bootstrap.widgets.TbListView',
    [
        'dataProvider' => $dataProvider,
        'viewData' => [
                    'isdelete' => true
                ],
        'id' => 'product-box',
        'itemView' => '//store/product/_single',
        'emptyText' => 'У Вас пока нет избранных товаров.',
        'summaryText' => "{count} тов.",
        'template' => '
                            <h1>Избранные товары</h1>
                            <div class="catalog-controls">
                                <div class="catalog-controls__sort">
                                    <div class="sort-box">
                                        <div class="sort-box__header">Сортировать по: </div>
                                        <div class="sort-box__body sort-box-wrapper">
                                            <div class="sort-box-wrapper__header">Сначала недорогие<i class="fa fa-angle-down" aria-hidden="true"></i></div>
                                            <div class="sort-box-wrapper__body">
                                                <div class="sort-box-wrapper__link" data-href="?sort=price">Сначала недорогие</div>
                                                <div class="sort-box-wrapper__link" data-href="?sort=price.desc">Сначала дорогие</div>
                                            </div>
                                        </div>
                                        <div class="sort-box-wrapper__link of_news" data-href="?is_new=1">Новинки</div>
                                        <!-- <div class="sort-box-wrapper__link of_news" data-href="?is_sale">Скидки</div>-->
                                    </div>
                                </div>
                                <div class="catalog-controls__res">
                                    {summary}
                                </div>
                            </div>
                            {items}
                            {pager}
                        ',
        'sortableAttributes' => [
            'sku',
            'name',
            'price',
            'update_time',
        ],
        'itemsCssClass' => 'product-box product-list favor',
        'htmlOptions' => [
            // 'class' => 'product-box'
        ],
        'ajaxUpdate' => true,
        'enableHistory' => false,
        // 'ajaxUrl'=>'GET',
        'pagerCssClass' => 'pagination-box',
        'pager' => [
            'header' => '',
            'lastPageLabel' => '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
            'firstPageLabel' => '<i class="fa fa-angle-double-left" aria-hidden="true"></i>',
            // 'lastPageLabel'  => false,
            // 'firstPageLabel' => false,
            'prevPageLabel' => '<i class="fa fa-angle-left" aria-hidden="true"></i>',
            'nextPageLabel' => '<i class="fa fa-angle-right" aria-hidden="true"></i>',
            'maxButtonCount' => 5,
            'htmlOptions' => [
                'class' => 'pagination',
            ],
        ],
    ]
);?>
            </div>
        </div>
<div class="container" style="height: 40px"></div>



