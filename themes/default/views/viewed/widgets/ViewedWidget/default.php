<?php if (Yii::app()->viewed->count() > 0): ?>
<div class="viewed">
    <div class="container container-pr">
        <div class="brend-image">
            <div class="brend-image__img">
                <?= CHtml::image(Yii::app()->getTheme()->getAssetsUrl() . '/images/red.png','REDKIN') ?>
            </div>
        </div>
    <div class="product-section box-style">
                <div class="product-section__h2 box-style__h2">
                    <span>Ранее просмотренные товары</span>
                    <?= CHtml::link('Все товары', '/store') ?>
                </div>
            <div class="product-box product-box-carousel">
                <?php foreach ($products as $key => $data) : ?>
                    <div>
                        <?php Yii::app()->controller->renderPartial('//store/product/_viewed', ['data' => $data]) ?>
                    </div>
                <?php endforeach; ?>
            </div>
    </div>
  </div>
</div>
<?php endif ?>
