<div class="product-box__item__wrap">
<div class="product-box__item">
    <div class="product-item__head">
    <?php if ($data->is_popular): ?>
        <div class="best">Лучшее</div>
    <?php endif ?>
    <?php if ($data->is_new): ?>
        <div class="best new">Новинка</div>
    <?php endif ?>
    <?php if ($data->is_sale): ?>
        <div class="best sale">Акция</div>
    <?php endif ?>
      <?php if(Yii::app()->hasModule('favorite')):?>
            <div class="product-button__item product-favorite">
                <?php $this->widget('application.modules.favorite.widgets.FavoriteControl', [
                    'product' => $data,
                    'view' => "favorite-item"
                ]);?>
            </div>
        <?php endif;?>
    </div>

        <div class="product-box__img slickInslick" style="position: relative">
            <?php $images = $data->getImages(); ?>
            <?php foreach ($images as $key => $image) : ?>
                    <a  href="<?= ProductHelper::getUrl($data); ?>">
                        <?= CHtml::image($image->getImageUrl(259,248,true), '', [
                            'class' => 'gallery-image'
                        ])?>
                    </a>

            <?php endforeach ?>

        </div>
         <?php Yii::app()->getClientScript()->registerScript(
                    "slickInslick",
                    "
                        $('.slickInslick').slick({
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            fade: true,
                            dots: true,
                            arrows: false,
                            responsive: [
                                {
                                    breakpoint: 480,
                                    settings: {
                                        arrows: false,
                                    }
                                }
                            ]
                        });
                "
                ); ?>


    <div class="product-box__info">

            <div class="product-box__name">
                <span><?= $data->name; ?></span>
            </div>
            <div class="category-name">
                <?= $data->category->name; ?>
            </div>
        <div class="product-box__bottom">

                    <form action="<?= Yii::app()->createUrl('cart/cart/add'); ?>" method="post" data-max-value='<?= (int)$data->quantity ?>'>
                    <input type="hidden" name="Product[id]" value="<?= $data->id; ?>"/>
                    <?= CHtml::hiddenField(
                        Yii::app()->getRequest()->csrfTokenName,
                        Yii::app()->getRequest()->csrfToken
                    ); ?>

                    <div class="by-order ">
                        <div class="product-view__price product-price <?= ($data->hasDiscount()) ? 'product-price__new' : '' ?>" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                        <input type="hidden" id="base-price" value="<?= round($data->getResultPrice(), 2); ?>"/>
                        <div class="prices-spans">

                        <div class="product-price__result <?= $data->hasDiscount()? 'sale-price': ''?>">
                            <?php if ($data->hasDiscount()) : ?>
                            <div class="product-price__old">
                                <span><?= round($data->getResultPrice(), 2) ?></span>
                            </div>
                            <div class="price-result" itemprop="price" id="result-price<?= $data->id?>"><?= round($data->getBasePrice(), 2); ?>
                                <span class="ruble">руб.</span>
                            </div>
                            <?php else: ?>
                                <div class="price-result" itemprop="price" id="result-price<?= $data->id?>"><?= round($data->getBasePrice(), 2); ?>
                                   <span class="ruble">руб.</span>
                                </div>
                             <?php endif; ?>
                        </div>


                        </div>
                        <meta itemprop="priceCurrency" content="<?= Yii::app()->getModule('store')->currency?>">
                            <?= $data->isInStock() ? '<link itemprop="availability" href="http://schema.org/InStock">' : '<link itemprop="availability" href="http://schema.org/PreOrder">';?>
                    </div>

                                <?php if (Yii::app()->hasModule('order')) : ?>

                                <?php if (Yii::app()->hasModule('cart')) : ?>
                                    <div class="product-button__item product-cart">
                                        <button class="btn but but-add-cart js-add-product-to-cart">
                                            <span>купить</span>
                                        </button>
                                    </div>
                                <?php endif; ?>

                        </div>
                        <div class="product-view__hidden hidden">
                            <span id="product-result-price"><?= round($data->getResultPrice(), 2); ?></span> x
                            <span id="product-quantity">1</span> =
                            <span id="product-total-price"><?= round($data->getResultPrice(), 2); ?></span>
                            <span class="ruble"> <?= Yii::t("StoreModule.store", Yii::app()->getModule('store')->currency); ?></span>
                        </div>
                    <?php endif; ?>
                </form>

        </div>
    </div>

</div>
</div>