<?php /* @var $dataProvider CActiveDataProvider */ ?>
<?php if ($dataProvider->getTotalItemCount()): ?>
    <div class="viewed">
    <div class="container container-pr">
    <div class="brend-image">
        <div class="brend-image__img">
            <?= CHtml::image(Yii::app()->getTheme()->getAssetsUrl() . '/images/red.png','REDKIN') ?>
        </div>
    </div>
   <div class="product-section box-style">
                <div class="product-section__h2 box-style__h2">
                    <span>Вам могут понравиться</span>
                    <?= CHtml::link('Все товары', '/store') ?>
                </div>

                <?php $this->widget(
                    'zii.widgets.CListView',
                    [
                        'dataProvider' => $dataProvider,
                        'template' => '{items}',
                        'itemView' => '_item',
                        'cssFile' => false,
                        'pager' => false,
                        'itemsCssClass' => 'product-box product-box-carousel'
                    ]
                ); ?>

    </div>
   </div>
</div>
<?php endif; ?>

