<?php
/**
 * @var Product $product
 */
?>
<div class="col-sm-1"></div>
<div class="col-sm-10">
    <div class="photo">
        <a href="<?= ProductHelper::getUrl($data); ?>">
            <img src="<?= $product->getImageUrl(190, 190, false); ?>"/>
        </a>
    </div>
    <div class="info separator">
        <div class="product-atributes">
                <div class="product-section__h2 box-style__h2">
                    <span>Размеры</span>
                </div>
            <div class="product-atributes__box">

                    <?php foreach ($data->getAttributeGroups() as $groupName => $items) :
                                { ?>

                                            <?php
                                            foreach ($items as $attribute) :
                                                {
                                                $value = $data->attribute($attribute);
                                                    if($attribute->out){
                                                        continue;
                                                    }
                                                if (empty($value)) {
                                                    continue;
                                                }
                                                ?>

                                                   <label class="atributes_wrap">
                                                    <div class="key">
                                                        <input type="checkbox" name="product-<?= $data->id; ?>"value="<?= AttributeRender::renderValue($attribute, $data->attribute($attribute)); ?>" id="<?= $data->id; ?>">
                                                    </div>
                                                        <div class="value">
                                                            <?= AttributeRender::renderValue($attribute, $data->attribute($attribute)); ?>
                                                        </div>
                                                   </label >
                                                <?php }
                                            endforeach; ?>


                                <?php }
                            endforeach; ?>

            </div>
    </div>
    </div>
</div>
<div class="col-sm-1"></div>