<?php if($tree) : ?>
	<div class="product-atributes">
				<div class="product-section__h2 box-style__h2">
					<span>Размеры</span>
				</div>
			<div class="product-atributes__box">
				<?php foreach ($tree as $key => $data) : ?>
					<?php foreach ($data->getAttributeGroups() as $groupName => $items) :
                                { ?>

                                            <?php
                                            foreach ($items as $attribute) :
                                                {
                                                $value = $data->attribute($attribute);
                                                    if($attribute->out){
                                                        continue;
                                                    }
                                                if (empty($value)) {
                                                    continue;
                                                }
                                                ?>

                                                   <label class="atributes_wrap">
                                                    <div class="key">
                                                        <input type="checkbox" name="product-<?= $data->id; ?>"value="<?= AttributeRender::renderValue($attribute, $data->attribute($attribute)); ?>" id="<?= $data->id; ?>">
                                                    </div>
                                                        <div class="value">
                                                            <?= AttributeRender::renderValue($attribute, $data->attribute($attribute)); ?>
                                                        </div>
                                                   </label >
                                                <?php }
                                            endforeach; ?>


                                <?php }
                            endforeach; ?>
				<?php endforeach; ?>
			</div>
	</div>
<?php endif; ?>
