
<div class="catalog-home">
	<?php $count = 0; ?>
	<?php foreach ($tree as $item): ?>
		<?php $count++; ?>
			<a class="catalog-home__item" href="<?= $item['url']; ?>">
					<span class="catalog-home__img">
					<span class="qn n-1"></span>
					<span class="qn n-2"></span>
					<span class="qn n-3"></span>
					<span class="qn n-4"></span>
					<img  src="" data-src="<?= $item['icon']; ?>" alt="">
				    </span>
				    <span class="catalog-home__name">
				    	<?= $item['label']; ?>
					</span>
					<span class="catalog-home__description">
						<?php if ($item['count'] > 0): ?>
				        <span>Более <?= $item['count'] ?> товаров</span>
						<?php endif ?>

 
					</span>
			</a>
			<?php if ($count === 9){break;} ?>


	<?php endforeach; ?>
	</div>