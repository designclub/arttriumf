<?php if($tree) : ?>
	<div class="filter-block filter-block__categoryes">
	    <div class="filter-block__body filter-block__category">
			<div class="filter-block__list category-list-filter">
				<?php $count = 1; ?>
				    <?php foreach ($tree as $item): ?>
				    	<?php $count++ ?>
                <div class="category-list-filter__item  <?= ($count <= 6) ? '' : 'hidden'; ?>">
				        <?= CHtml::checkBox('categiry-'.$item['id'],Yii::app()->attributesFilter->isMainSearchParamChecked(AttributeFilter::MAIN_SEARCH_PARAM_CATEGORY, $item['id'], Yii::app()->getRequest()),['value' => $item['id'], 'id' => 'category_'. $item['id']]);?>
	                 <?= CHtml::label($item['label'], 'category_'. $item['id']);?>
                </div>
			    <?php endforeach; ?>
			    <?php if($count > 6) : ?>
				    <a class="filter-block__link" href="#">
				        <span data-text="Свернуть">Показать все</span>
				    </a>
				<?php endif; ?>
			</div>
		</div>
	</div>
<?php endif; ?>
