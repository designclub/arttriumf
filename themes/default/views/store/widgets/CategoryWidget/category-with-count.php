<?php if($tree) : ?>
	<div class="filter-block">
		<div class="filter-block__header"><?= Yii::t('StoreModule.store', 'Categories') ?></div>
	    <div class="filter-block__body filter-block__category">
			<ul class="filter-block__list category-list-filter">
			    <?php $count = 1; ?>
			    <?php foreach ($tree as $item): ?>
			        <li class="filter-block__item category-list-filter__item filter-list <?= ($count <= 6) ? '' : 'hidden'; ?>">
			        	<a href="<?= $item['url']; ?>">
				            <span class="category-list-filter__name"><?= $item['label']; ?></span>
				            <span class="category-list-filter__count">(<?= $item['count'] ?>)</span>
			            </a>
			        </li>
					<?php $count++; ?>
			    <?php endforeach; ?>
				<?php if($count > 6) : ?>
				    <a class="filter-block__more" href="#">
				        <span data-text="Скрыть">Показать еще (<?= $count - 7 ?>)</span>
				    </a>
				<?php endif; ?>
			</ul>
		</div>
	</div>
<?php endif; ?>
