<?php $filter = Yii::app()->getComponent('attributesFilter');?>
<div class="filter-block price">
    <div class="filter-block__header"><?= Yii::t('StoreModule.store', 'Price'); ?></div>
    <div class="range filter-block__body filter-block-price">
        <?= CHtml::textField('price[from]', Yii::app()->attributesFilter->getMainSearchParamsValue('price', 'from', Yii::app()->getRequest()), [
            'class' => 'form-control js-from',
            'placeholder' => Yii::t('StoreModule.store', 'from')
        ]); ?>
        <?= CHtml::textField('price[to]', Yii::app()->attributesFilter->getMainSearchParamsValue('price', 'to', Yii::app()->getRequest()), [
            'class' => 'form-control js-to',
            'placeholder' => Yii::t('StoreModule.store', 'to')
        ]); ?>
    </div>
    <div class="range">
        <input type="text" class="js-range-price" name="my_range" value=""
            data-type="double"
            data-min="0"
            data-max="<?= ceil($cost['price']); ?>"
            data-from="0"
            data-to="<?= ceil($cost['price']); ?>"
            data-grid="true"
            data-skin="round"
        />
    </div>
</div>
