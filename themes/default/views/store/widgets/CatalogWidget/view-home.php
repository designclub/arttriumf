<?php if($category): ?>
	<div class="catalog-home">
	<?php foreach ($category as $key => $data) : ?>
			<a class="catalog-home__item" href="<?= $data->getCategoryUrl(); ?>">
					<span class="catalog-home__img">
					<?php if($data->image) : ?>
						<?= CHtml::image($data->getImageUrl(), ''); ?>
					<?php else: ?>
						<?= CHtml::image(Yii::app()->getTheme()->getAssetsUrl() . '/images/nophoto-product.jpg', ''); ?>
					<?php endif; ?>
					<span class="qn n-1"></span>
					<span class="qn n-2"></span>
					<span class="qn n-3"></span>
					<span class="qn n-4"></span>

				    </span>
				    <span class="catalog-home__name">
				    	<?= $data->name; ?>
					</span>
					<span class="catalog-home__description">
						<?= $data->name_short; ?>
					</span>
			</a>
	<?php endforeach; ?>
	</div>
<?php endif; ?>

