<?php if($category): ?>
    <div class="category-sub">
    <?php foreach ($category as $key => $data) : ?>
            <a class="category-sub__item" href="<?= $data->getCategoryUrl(); ?>">
                    <span class="category-sub__img">
                    <?php if($data->image) : ?>
                        <?= CHtml::image($data->getImageUrl(300,277,true), ''); ?>
                    <?php else: ?>
                        <?= CHtml::image(Yii::app()->getTheme()->getAssetsUrl() . '/images/nophoto-product.jpg', ''); ?>
                    <?php endif; ?>
                    <span class="qn n-1"></span>
                    <span class="qn n-2"></span>
                    <span class="qn n-3"></span>
                    <span class="qn n-4"></span>
                    </span>
                    <span class="category-sub__name">
                        <?= $data->name; ?>
                    </span>
                    <span class="catalog-sub__description">
                        <span>Более <?= $data->getCountProduct() ?> товаров</span>
                    </span>

            </a>
    <?php endforeach; ?>
    </div>
<?php endif; ?>
