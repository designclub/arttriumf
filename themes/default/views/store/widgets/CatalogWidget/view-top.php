<?php if($category): ?>
	<div class="catalog_title" style="display: none"><span>Каталог</span></div>
	<div class="catalog-menu">
	<?php foreach ($category as $key => $data) : ?>
			<a class="catalog-menu__item" href="<?= $data->getCategoryUrl(); ?>">
					<span class="catalog-menu__img">
					<?php if($data->image) : ?>
						<?= CHtml::image($data->getImageUrl(100,100,true), ''); ?>
					<?php endif; ?>

				    </span>
				    <span class="catalog-menu__name">
				    	<?= $data->name; ?>
					</span>
			</a>
	<?php endforeach; ?>
	</div>
<?php endif; ?>

