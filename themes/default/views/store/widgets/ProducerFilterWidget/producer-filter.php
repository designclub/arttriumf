<?php if (!empty($producers)) :?>
    <div class="filter-block">
        <div class="filter-block__header"><?= Yii::t('StoreModule.store', 'Производитель');?></div>
        <div class="filter-block__body filter-block__producer">
            <div class="filter-block__list">
                <?php $count = 1; ?>
                <?php foreach ($producers as $producer) :?>
                    <div class="filter-block__item filter-checkbox filter-list <?= ($count <= 6) ? '' : 'hidden'; ?>">
                        <?= CHtml::checkBox(
                            'brand[]',
                            Yii::app()->attributesFilter->isMainSearchParamChecked(
                                AttributeFilter::MAIN_SEARCH_PARAM_PRODUCER,
                                $producer->id,
                                Yii::app()->getRequest()
                            ),
                            [
                                'value' => $producer->id,
                                'id' => 'brand_'.$producer->id
                            ]
                        );
                        ?>
                        <?= CHtml::label($producer->name, 'brand_'.$producer->id);?>
                    </div>
                    <?php $count++; ?>
                <?php endforeach;?>
                <?php if($count > 6) : ?>
                    <a class="filter-block__more" href="#">
                        <span data-text="Скрыть">Показать еще (<?= $count - 7 ?>)</span>
                    </a>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php endif;?>
