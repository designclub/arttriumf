<?php $form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm',
    [
        'action' => ['/store/product/index'],
        'method' => 'GET',
        'htmlOptions' => [
            'class'  => 'form-inline form-search'
        ]
    ]
) ?>
    <div class="input-group search-input-group">

            <button type="submit">
                <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/magnifer.svg'); ?>
            </button>

        <?= CHtml::textField(AttributeFilter::MAIN_SEARCH_QUERY_NAME, CHtml::encode(Yii::app()->getRequest()->getQuery(AttributeFilter::MAIN_SEARCH_QUERY_NAME)),
            [
                'class'        => 'form-control',
                'autocomplete' => 'off',
                'placeholder'  => 'Поиск по ключевым словам'
            ]
        ); ?>
    </div>
<?php $this->endWidget(); ?>

<div class="search-xs hide">
    <a class="search-xs-link" href="#" data-toggle="modal" data-target="#searchModal">
        <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/magnifer.svg'); ?>
    </a>
</div>