<?php
$mainAssets = Yii::app()->getTheme()->getAssetsUrl();
/* @var $category StoreCategory */
// $category->getMetaDescription()
$this->title = $category->getMetaTitle() ?: CHtml::encode($category->getTitle()) . ' - купить в Салехарде';
$this->description = $category->getMetaDescription() ?: CHtml::encode($category->getTitle()) . ': купить в интернет-магазине «Арт Триумф» в Салехарде. ' . CHtml::encode($category->getTitle()) . ' - заказать по низким ценам. Высокое качество и большой ассортимент!';
$this->keywords = $category->getMetaKeywords();
$this->canonical = $category->getMetaCanonical() ? $category->getMetaCanonical() : $this->canonical;
$this->title_short = CHtml::encode($category->getTitle());
$this->breadcrumbs = [Yii::t("StoreModule.store", "Catalog") => ['/store/product/index']];
$this->breadcrumbs = array_merge(
    $this->breadcrumbs,
    $category->getBreadcrumbs(true)
);
?>
<div class="container catalog-container">
    <h1 class="page_title">
        <?= $this->title_short?>
    </h1>
</div>
<div class="page-content category-content">
    <div class="container">
        <div class="category-box">
            <div class="category-box__sidebar">
                <div class="sidebar-box">
                    <div class="sidebar-box__close"><span></span></div>
                    <form id="store-filter" name="store-filter" method="get">
                        <div class="close"></div>
                        <div class="filter-content left-sidebar">

                            <?php $this->widget('application.modules.store.widgets.CategoryWidget', [
                                'depth' => 3
                            ]);?>
                        </div>
                    </form>
                </div>
            </div>
            <div class="category-box__content">
                <div id="selected-filters" class="selected-filters"></div>
                <?php
$this->widget(
    // 'application.components.MyClistView',
    'bootstrap.widgets.TbListView',
    [
        'dataProvider' => $dataProvider,
        'id' => 'product-box',
        'itemView' => '//store/product/_single',
        'emptyText' => 'В данной категории нет товаров.',
        'summaryText' => "{count} тов.",
        'template' => '
                <div class="catalog-controls">
                    <div class="catalog-controls__sort">
                        <div class="sort-box">
                            <div class="sort-box__header">Сортировать по: </div>
                            <div class="sort-box__body sort-box-wrapper">
                                <div class="sort-box-wrapper__header">Сначала недорогие<i class="fa fa-angle-down" aria-hidden="true"></i></div>
                                <div class="sort-box-wrapper__body">
                                    <div class="sort-box-wrapper__link" data-href="?sort=price">Сначала недорогие</div>
                                    <div class="sort-box-wrapper__link" data-href="?sort=price.desc">Сначала дорогие</div>
                                </div>
                            </div>
                                 <div class="sort-box-wrapper__link of_news" data-href="?is_new=1">Новинки</div>
                            <!-- <div class="sort-box-wrapper__link of_news" data-href="?is_sale">Скидки</div>-->
                        </div>
                    </div>
                    <div class="catalog-controls__res">
                        {summary}
                    </div>
                </div>
                {items}
                {pager}
            ',
        'sortableAttributes' => [
            'sku',
            'name',
            'price',
            'update_time',
        ],
        'itemsCssClass' => 'product-box product-list',
        'htmlOptions' => [
            'class' => 'product-box'
        ],
        'ajaxUpdate' => true,
        'enableHistory' => true,
        'ajaxUrl'=>'GET',
        'pagerCssClass' => 'pagination-box',
        'pager' => [
            'header' => '',
            'lastPageLabel' => '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
            'firstPageLabel' => '<i class="fa fa-angle-double-left" aria-hidden="true"></i>',
            'prevPageLabel' => '<i class="fa fa-angle-left" aria-hidden="true"></i>',
            'nextPageLabel' => '<i class="fa fa-angle-right" aria-hidden="true"></i>',
            'maxButtonCount' => 5,
            'htmlOptions' => [
                'class' => 'pagination',
            ],
        ],
    ]
);?>
            </div>
        </div>
    </div>
</div>
<?php if ($category->description): ?>
    <div class="container">
        <div class="categories-description" style="margin: 30px 0;">
            <?= $category->description; ?>
        </div>
    </div>
<?php endif ?>
<div class="container hr"></div>
<noindex>
<?php $this->renderPartial('//layouts/_bests'); ?>
</noindex>