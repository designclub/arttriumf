<?php
$mainAssets = Yii::app()->getTheme()->getAssetsUrl();
/* @var $category StoreCategory */
$this->title = $category->getMetaTitle() ?: CHtml::encode($category->getTitle()) . ' - купить в Салехарде';
$this->description = $category->getMetaDescription() ?: CHtml::encode($category->getTitle()) . ': купить в интернет-магазине «Арт Триумф» в Салехарде. ' . CHtml::encode($category->getTitle()) . ' - заказать по низким ценам. Высокое качество и большой ассортимент!';
$this->keywords = $category->getMetaKeywords();
$this->canonical = $category->getMetaCanonical() ? $category->getMetaCanonical() : $this->canonical;
$this->title_short = CHtml::encode($category->getTitle());
$this->breadcrumbs = [Yii::t("StoreModule.store", "Catalog") => ['/store/product/index']];
$this->breadcrumbs = array_merge(
    $this->breadcrumbs,
    $category->getBreadcrumbs(true)
);
?>

<div class="container catalog-container">
    <h1 class="page_title">
        <?= $this->title_short?>
    </h1>
</div>
<section class="categoryies-content">
    <div class="container d-flex">
        <div class="left-sidebar">
            <?php $this->widget('application.modules.store.widgets.CategoryWidget', ['view' => 'category-widget','depth' => 2]); ?>
        </div>
        <div class="categories_box-offParent">
        <?php $this->widget('application.modules.store.widgets.CatalogWidget', ['view' => 'view-parent', 'category_id' => $category->id]); ?>
        </div>
    </div>
</section>
<?php if ($category->description): ?>
    <div class="container">
        <div class="categories-description" style="margin: 30px 0;">
            <?= $category->description; ?>
        </div>
    </div>
<?php endif ?>
<div class="container hr"></div>
<noindex>
<?php $this->renderPartial('//layouts/_bests'); ?>
</noindex>
