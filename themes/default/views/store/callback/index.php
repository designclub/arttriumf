<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
    'id' => 'callback-product',
    'type' => 'vertical',
    'htmlOptions' => [
        'class' => 'form-product',
        'data-type' => 'ajax-form',
    ],
]); ?>
    <?php if (Yii::app()->user->hasFlash('success')): ?>
        <script>
            ym('56431171', 'reachGoal', 'cena');
        </script>
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Спасибо за заказ!</h4>
        </div>
        <div class="alert alert-success"><?= Yii::app()->user->getFlash('success') ?></div>
    <?php else: ?>
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel"><?= $product->name ?></h4>
            <div class="description-message">
                Заполните форму<br>
                в ближайшее время мы свяжемся с Вами
            </div>
        </div>
        <div class="modal-body">
            <?= $form->textFieldGroup($model, 'name') ?>
            <?= $form->telFieldGroup($model, 'phone', [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'data-mask' => "phone"
                    ]
                ]
            ]) ?>
        </div>
        <div class="modal-footer">
            <button id="prod-button" class="calling" data-send="ajax" >Отправить</button>
        </div>
    <?php endif ?>
<?php $this->endWidget() ?>
