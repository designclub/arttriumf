<?php
$mainAssets = Yii::app()->getTheme()->getAssetsUrl();

/* @var $category StoreCategory */

$this->title = Yii::app()->getModule('store')->metaTitle ?: Yii::t('StoreModule.store', 'Catalog');
$this->description = Yii::app()->getModule('store')->metaDescription;
$this->keywords = Yii::app()->getModule('store')->metaKeyWords;
$this->title_short = Yii::t("StoreModule.store", "Catalog");
$this->breadcrumbs = [Yii::t("StoreModule.store", "Catalog")];
?>

<div class="container catalog-container">
    <h1 class="page_title">
        <?= $this->title_short?> &laquo;АРТ ТРИУМФ&raquo;
    </h1>
</div>
<section class="categoryies-content">
    <div class="container d-flex">
        <div class="left-sidebar transparent">
            <?php $this->widget('application.modules.store.widgets.CategoryWidget',['view' => 'category-widget','depth' => 2]); ?>
        </div>
        <div class="categories_box-offParent">
        <?php $this->widget('application.modules.store.widgets.CatalogWidget',['view' => 'view-parent-index']); ?>
        </div>
    </div>
</section>
<div class="container hr"></div>
<?php $this->renderPartial('//layouts/_bests'); ?>