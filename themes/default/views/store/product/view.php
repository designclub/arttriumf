<?php

/* @var $product Product */
// $product->getMetaTitle()
// $product->getMetaDescription()
    

$this->title = $product->getMetaTitle() ?: CHtml::encode($product->category->getTitle()) . ' - ' . CHtml::encode($product->getTitle()) . ' - купить в Салехарде';


$this->description = $product->getMetaDescription() ?: 'Купить ' . CHtml::encode(mb_strtolower($product->category->getTitle())) . ' - ' . CHtml::encode($product->getTitle()) . ' в Салехарде. Заказать ' . CHtml::encode($product->getTitle()) . ' по низкой цене в интернет-магазине «Арт Триумф». Высокое качество и большой ассортимент!';
$this->keywords = $product->getMetaKeywords();
$this->canonical = $product->getMetaCanonical() ? $product->getMetaCanonical() : $this->canonical;

$mainAssets = Yii::app()->getModule('store')->getAssetsUrl();

$this->breadcrumbs = array_merge(
    [Yii::t("StoreModule.store", 'Catalog') => ['/store/product/index']],
    $product->category ? $product->category->getBreadcrumbs(true) : [],
    [CHtml::encode($product->name)]
);
?>
<div class="page-content product-view-content" itemscope="" itemtype="http://schema.org/Product">
    <div class="container">
        <div class="product-view">
            <div class="product-view__info">
                <div class="product-view__type product-type">
                    <?php foreach ($product->getBadges() as $key => $badge) : ?>
                        <div class="product-type__color">
                            <?= CHtml::image(Yii::app()->getTheme()->getAssetsUrl() . '/images/icon/'.$badge, ''); ?>
                            <span><?= $product->getAttributeLabel($key); ?></span>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="clearfix"></div>
                <h1 itemprop="name" ><?= CHtml::encode($product->getTitle()); ?></h1>
                <form action="<?= Yii::app()->createUrl('cart/cart/add'); ?>" method="post" data-max-value='<?= (int)$product->quantity ?>'>
                    <input type="hidden" name="Product[id]" value="<?= $product->id; ?>"/>
                    <?= CHtml::hiddenField(
                        Yii::app()->getRequest()->csrfTokenName,
                        Yii::app()->getRequest()->csrfToken
                    ); ?>
                        <?php //Цена ?>
                        <?php if ($product->getBasePrice() > 0): ?>
                    <div class="product-view__price product-price <?= ($product->hasDiscount()) ? 'product-price__new' : '' ?>"  itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
                        <input type="hidden" id="base-price" value="<?= round($product->getBasePrice(), 2); ?>"/>
                        <div class="product-view__price_heading">Цена:</div>
                        <span class="product-price__result">
                            <span class="price-result" itemprop="price" id="result-price<?= $product->id?>"><?= round($product->getBasePrice(), 2); ?></span>
                            <span class="ruble">&#8381;</span>
                        </span>
                        <?php if ($product->hasDiscount()) : ?>
                            <span class="product-price__old">
                                <span class="price-old"><?= round($product->getResultPrice(), 2) ?></span>
                            </span><span class="ruble"></span><br>
                        <?php endif; ?>
                        <meta itemprop="priceCurrency" content="<?= Yii::app()->getModule('store')->currency?>">
                            <?= $product->isInStock() ? '<link itemprop="availability" href="http://schema.org/InStock">' : '<link itemprop="availability" href="http://schema.org/PreOrder">';?>
                    </div>
                    <?php else: ?>
                        <div itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
                            <div class="" itemprop="price" style="height: 30px; opacity: 0;">0</div>
                            <meta itemprop="priceCurrency" content="<?= Yii::app()->getModule('store')->currency?>">
                            <?= $product->isInStock() ? '<link itemprop="availability" href="http://schema.org/InStock">' : '<link itemprop="availability" href="http://schema.org/PreOrder">';?>
                        </div>
                    <?php endif; ?>
                    <div class="tooltips">Данный товар в наличии

                    </div>
                        <div class="spin-cart">
                         <?php if ($product->getBasePrice() > 0): ?>

                                <div class="product-view__spinput">
                                    <?php
                                        $minQuantity = 1;
                                        $maxQuantity = Yii::app()->getModule('store')->controlStockBalances ? $product->getAvailableQuantity() : 99;
                                    ?>
                                    <span data-min-value='<?= $minQuantity; ?>' data-max-value='<?= $maxQuantity; ?>'
                                          class="spinput js-spinput">
                                        <span class="spinput__minus js-spinput__minus product-quantity-decrease"></span>
                                        <input name="Product[quantity]" value="1" class="spinput__value product-quantity-input"
                                               id="product-quantity-input"/>
                                        <span class="spinput__plus js-spinput__plus product-quantity-increase"></span>
                                    </span>
                                    <span class="boxes">Товаров</span>
                                </div>

                                    <?php if (Yii::app()->hasModule('cart')) : ?>
                                            <button class="btn-cart site_btn js-add-product-to-cart">
                                                <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/cart.svg'); ?>
                                                <span>В корзину</span>
                                            </button>
                                    <?php endif; ?>
                                    <a href="#" data-target = "#CallbackFormEmail" data-toggle = "modal" class="calling js-button">Задать вопрос</a>

                        <?php else: ?>

                            <a href="<?= Yii::app()->createUrl('/store/callback/index', ['id' => $product->id]) ?>" class="calling ml0 js-callback-modal">Узнать цену</a>

                        <?php endif; ?>
                        </div>

                        <div class="params-preview">
                            <!-- <div class="params-preview__title">Параметры:</div>
                            <div class="params-preview__content-ofTable">

                            </div> -->
                            <a href="#tab-params" class="hd-md" id="js-tab-params"><span>Показать все параметры</span><i class="fa fa-angle-down" aria-hidden="true"></i></a>
                        </div>
                        <div class="product-view__hidden hidden">
                            <span id="product-result-price"><?= round($product->getResultPrice(), 2); ?></span> x
                            <span id="product-quantity">1</span> =
                            <span id="product-total-price"><?= round($product->getResultPrice(), 2); ?></span>
                            <span class="ruble"> <?= Yii::t("StoreModule.store", Yii::app()->getModule('store')->currency); ?></span>
                        </div>


                </form>
            </div>
            <?php $images = $product->getImages(); ?>
            <div class="product-view__img <?= (count($images) > 0) ? '' : 'product-view__img_2'; ?>">
                    <?php if(Yii::app()->hasModule('favorite')):?>
                        <div class="product-button__item product-favorite">
                            <?php $this->widget('application.modules.favorite.widgets.FavoriteControl', [
                                'product' => $product,
                                'view' => "favorite-item"
                            ]);?>
                        </div>
                    <?php endif;?>
                <div class="image-preview">
                    <div>
                        <div class="image-preview__img">
                            <a data-fancybox="image" href="<?= StoreImage::product($product); ?>">
                                <img
                                    class="gallery-image"
                                    src="<?= StoreImage::product($product); ?>"
                                    itemprop="image"
                                />
                            </a>
                        </div>
                    </div>
                    <?php foreach ($images as $key => $image) : ?>
                        <div>
                            <div class="image-preview__img">
                                <a data-fancybox="image" href="<?= $image->getImageUrl(); ?>">
                                    <?= CHtml::image($image->getImageUrl(579,485,true), '', [
                                        'class' => 'gallery-image'
                                    ])?>
                                </a>
                            </div>
                        </div>
                    <?php endforeach ?>
                </div>
                <?php Yii::app()->getClientScript()->registerScript(
                    "product-image-preview",
                    "
                        $('.image-preview').slick({
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            fade: true,
                            dots: false,
                            arrows: false,
                            asNavFor: '.image-thumbnail',
                            responsive: [
                                {
                                    breakpoint: 480,
                                    settings: {
                                        arrows: false,
                                    }
                                }
                            ]
                        });
                "
                ); ?>

                <!-- Миниатюры -->
                <?php if (count($images) > 0) : ?>
                    <div class="image-thumbnail">
                        <div>
                            <div class="image-thumbnail__box">
                                <div class="image-thumbnail__img">
                                    <img src="<?= StoreImage::product($product); ?>" />
                                </div>
                            </div>
                        </div>
                        <?php foreach ($images as $key => $image) : ?>
                            <div>
                                <div class="image-thumbnail__box">
                                    <div class="image-thumbnail__img">
                                        <?= CHtml::image($image->getImageUrl(183,130,true), '', ['style'=>''])?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach ?>
                    </div>
                    <?php Yii::app()->getClientScript()->registerScript(
                        "product-image-thumbnail",
                        "
                            $('.image-thumbnail').slick({
                                slidesToShow: 3,
                                slidesToScroll: 1,
                                asNavFor: '.image-preview',
                                dots: false,
                                arrows: false,
                                focusOnSelect: true,
                                responsive: [
                                    {
                                        breakpoint: 641,
                                        settings: {
                                            vertical: false,
                                            slidesToShow: 3,
                                            slidesToScroll: 1,
                                        }
                                    },
                                ]
                            });
                    "
                    ); ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="product-view-info" id="tab-params">
            <ul class="product-view-info__tabs" id="myTab">

                    <li><a href="#data" data-toggle="tab"><?= Yii::t("StoreModule.store", "Параметры"); ?></a></li>
                    <li>
                        <a href="#description" data-toggle="tab">Доставка и самовывоз</a>
                    </li>



            </ul>

            <div class="product-view-info__tab-content tab-content">

                <?php if (!empty($product->description)): ?>
                    <div class="tab-pane" id="data" itemprop="description">
                        <?= $product->description; ?>
                    </div>
                    <?php else: ?>
                    <div class="tab-pane" id="data" itemprop="description">
                        <table>
                            <tr>
                                <td>Описание</td>
                                <td>Цена</td>
                            </tr>
                            <tr>
                                <td>Описание</td>
                                <td>Цена</td>
                            </tr>
                            <tr>
                                <td>Описание</td>
                                <td>Цена</td>
                            </tr>
                        </table>
                    </div>
                <?php endif; ?>

                    <div class="tab-pane" id="description">
                        <div class="tab-description-text">
                            <span>Доставка</span>
                            От 2 до 3 недель до подьезда заказчика <br/>
                            Услуги грузчика - оплата договорная
                            <span>Самовывоз</span>
                            г. Салехард, ул. Республики, дом: 77
                            <a href="tel:+79028161971">+7 (902) 816 19 71</a>
                            <a href="tel:+73492261971">+7 (34922) 6 19 71 </a>
                        </div>

                        <div class="map_box">
                            <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3A32c78e0e23d691347a32fc6f874d2f5ccf3b490afc44ac44c7dd43ef07d7232a&amp;source=constructor" width="100%" height="500" frameborder="0"></iframe>
                        </div>
                    </div>

            </div>
        </div>
    </div>
</div>
<div class="bests">
<div class="tabs-wrapper">
<div class="container">

    <h2 class="page_title">Похожие товары</h2>
    <?php $this->widget('application.modules.store.widgets.ProductTypeWidget', [
                    'delete' => "id <> {$product->id}",
                    'category_id' => $product->category_id,

                ]); ?>
</div>
</div>
</div>
<?php
$fancybox = $this->widget(
    'gallery.extensions.fancybox3.AlFancybox',
    [
        'target' => '[data-fancybox]',
        'lang'   => 'ru',
        'config' => [
            'animationEffect' => "fade",
            'buttons' => [
                "zoom",
                "close",
            ]
        ],
    ]
);
?>
<?php Yii::app()->getClientScript()->registerScript(
    "product-myTab",
    <<<JS
        $("#myTab li").first().addClass('active');
        $(".tab-pane").first().addClass('active');

        var span = $('.product-view-info').find('table tr td');
        $(span).each(function(){
        var newText = $(this).text();
        newText = '<span>' + newText + '</span>';
        $(this).html(newText);
    });

JS
); ?>
<?php
Yii::app()->clientScript->registerScript("tooltips", "

    $('.show-params').on('click',function(e){
        e.preventDefault();
        let table = $('.product-view-info table'),
            text = $(this).data('text');
        table.find('tr').slice(+4).slideToggle();
        $(this).toggleClass('active');
        if($(this).hasClass('active')){
            $(this).text('Скрыть');
        }else{
            $(this).text(text);
        }
    })
  var paramsPrev = $('.params-preview__content-ofTable'),
      table = $('.product-view-info table').html(),
      paramsPreview = $('.params-preview__content-ofTable');
      paramsPreview.html('<table>' + table + '</table>').find('tr').slice(+4).hide();

      $('#js-tab-params').on('click',function(e){
        e.preventDefault();
        var targetLink = $(this).attr('href'),
            targetBox = $(targetLink).offset().top;
            $('html,body').animate({
                scrollTop:targetBox
                },500);
        })

");
 ?>