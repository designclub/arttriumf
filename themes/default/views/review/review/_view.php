<div class="review-page__items">
	<div class="review-page__img">
		<?php if($data->image){
                echo CHtml::image($data->getImageUrl(120, 120, true),'');
            }else{
                echo CHtml::image(Yii::app()->getTheme()->getAssetsUrl() . '/images/nophoto.jpg', '', ['class'=>'img-responsive']);
            }
        ?>
	</div>
	<div class="review-page__info">
		<div class="review-page__name">
			<span><?php echo CHtml::encode( $data->username); ?></span>
		</div>
		<div class="review-page__text">
			<p><?php echo $data->text; ?></p>
		</div>
	</div>
</div>
