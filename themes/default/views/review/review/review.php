
<?php

$this->title = "Отзывы";
$this->description =Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = Yii::app()->getModule('yupe')->siteKeyWords;
// $this->breadcrumbs = $this->getBreadCrumbs();
$this->breadcrumbs = ["Отзывы"];
require 'phpQuery.php';
?>

<div class="container">
		<!-- в этом месте, если что,хлебн.крошки -->
		<h1 class="text-center">Отзывы и оценки Яндекс</h1>
<?php
		$url = 'https://yandex.ru/ugcpub/pk/AaHsSwyvHYHA?main_tab=org';
        $file = file_get_contents($url);
        $doc = phpQuery::newDocument($file);
        $tr = $doc->find('.MyTabs-Content');
        echo "<div class='col-md-12'>";
        echo $tr;
        echo "</div>";

 ?>
		<!--<h1 class="text-center p7"><?= $this->title; ?> на сайте</h1>-->


		<?php $this->widget(
		    'bootstrap.widgets.TbListView',
		    [
		        'dataProvider' => $dataProvider,
		        'emptyText' => '',
		        'id' => 'review-box',
		        'itemView' => '_view',
		        'summaryText' => '',
		        'template'=>'{items} {pager}',
		        'itemsCssClass' => 'review-page',
		        'ajaxUpdate'=>'true',
		        'pagerCssClass' => 'pagination-box',
		        'pager' => [
		            'header' => '',
		            'lastPageLabel' => '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
		            'firstPageLabel' => '<i class="fa fa-angle-double-left" aria-hidden="true"></i>',
		            'prevPageLabel' => '<i class="fa fa-angle-left" aria-hidden="true"></i>',
		            'nextPageLabel' => '<i class="fa fa-angle-right" aria-hidden="true"></i>',
		            'maxButtonCount' => 5,
		            'htmlOptions' => [
		                'class' => 'pagination'
		            ],
		        ]
		    ]
		); ?>

</div>

<div class="review-form_wrap">
	<div class="container">
		<div class="review-form">
					<h2 class="text-center h1">Оставить отзыв</h2>
					<?php $this->widget('application.modules.review.widgets.ReviewWidget',['view' => 'reviewformwidget']); ?>
		</div>
	</div>
</div>
