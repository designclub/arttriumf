<div id="messageModal" class="modal fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Закрыть">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="modal-title">Уведомление</div>
            </div>
            <div class="modal-body">
                <p style="font-size: 18px;color:#0c4e76">Спасибо за Ваш отзыв!<br> после прохождения модерации,он будет доступен для просмотра.</p>
           </div>
        </div>
    </div>
</div>
<div id="reviewZayavkaModal" class="modal fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Закрыть">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="modal-title">Оставить отзыв</div>
            </div>
            <?php
                $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
                    'id'          => 'review-forms',
                    'type'        => 'vertical',
                    'htmlOptions' => ['class' => '',
                        'data-type' => 'ajax-form',
                        'enctype' => 'multipart/form-data'],
                ]); ?>

                    <?php if (Yii::app()->user->hasFlash('review-success')) : ?>
                         <script>
                            $('#messageModal').modal('show');
                            $('#reviewZayavkaModal').modal('hide');
                            setTimeout(function(){
                                $('#messageModal').modal('hide');
                            }, 4000);
                        </script>
                    <?php endif ?>

                    <div class="modal-body">
                        <?= $form->textFieldGroup($model, 'username', [
                            'widgetOptions' => [
                                'htmlOptions' => [
                                    'data-original-title' => $model->getAttributeLabel('username'),
                                    'data-content'        => $model->getAttributeDescription('username')
                                ],
                            ],
                        ]); ?>
                        <?= $form->textFieldGroup($model, 'useremail', [
                            'widgetOptions' => [
                                'htmlOptions' => [
                                    'data-original-title' => $model->getAttributeLabel('useremail'),
                                    'data-content'        => $model->getAttributeDescription('useremail'),
                                    'autocomplete' => 'off'
                                ],
                            ],
                        ]); ?>
                        <?= $form->textAreaGroup($model, 'text', [
                            'widgetOptions' => [
                                'htmlOptions' => [
                                    'data-original-title' => $model->getAttributeLabel('text'),
                                    'data-content'        => $model->getAttributeDescription('text')
                                ],
                            ],
                        ]); ?>

                        <div class="row">
                            <div class="captha-box col-sm-8">
                                <div class="captha-box__in2">
                                    <div class="form-captcha">
                                        <div class="g-recaptcha" data-sitekey="<?= Yii::app()->params['key'] ?>"></div>
                                        <?= $form->error($model, 'verifyCode');?>
                                   </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <?= CHtml::submitButton('Отправить', ['id' => 'reviewZayavkabutton', 'class' => 'btn btn-default reviewZayavka-form-button', 'data-send'=>'ajax'
                                ]) ?>
                            </div>
                        </div>

                    </div>
                <?php $this->endWidget(); ?>
        </div>
    </div>
</div>