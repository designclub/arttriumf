<div class="review-contents-widget">
    <div class="widget-header">
        <h2 class="page_title">
           Отзывы о нас
       </h2>
    </div>
        <div class="reviewBox review-carousel">
            <?php foreach ($model as $key => $item) : ?>

                    <div class="reviewBox__item">

                                <div class="revBody">
                                    <h4><?= $item->username; ?></h4>
                                    <div class="revText">
                                        <?= $item->text; ?>
                                    </div>
                                    <a href="#" class="sendReview  js-button" data-target="#reviewZayavkaModal" data-toggle="modal">Оставить отзыв</a>
                                </div>


                    </div>

            <?php endforeach; ?>
        </div>
</div>





