<div class="review-curs">
	<h2>Отзывы</h2>
	<div class="review-page">
		<?php foreach ($model as $key => $item) : ?>
			<?php Yii::app()->controller->renderPartial('//review/review/_view', ['data' => $item]) ?>
		<?php endforeach; ?>
	</div>
	<div class="review-curs__but">
	    <a class="review-curs__link but but-red" href="/review">Все Отзывы</a>
	</div>
</div>
