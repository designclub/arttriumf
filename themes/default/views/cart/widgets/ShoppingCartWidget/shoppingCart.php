<?php
$currency = Yii::app()->getModule('store')->currency;
?>
<div class="but-cart but-header js-cart" id="cart-widget" data-cart-widget-url="<?= Yii::app()->createUrl('/cart/cart/widget'); ?>">
    <?php if (!Yii::app()->cart->isEmpty()): ?>
        <a href="<?= Yii::app()->createUrl('/cart/cart/index') ?>">
    <?php endif; ?>
        <div class="but-cart__icon but-header__icon">
            <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/cart.svg'); ?>
            <div class="but-cart__count but-header__count <?= (Yii::app()->cart->isEmpty()) ? 'active' : ''; ?>">
                <?= Yii::app()->cart->getItemsCount(); ?>
            </div>
            <?php if (!Yii::app()->cart->isEmpty()): ?>
            <div class="but-cart__sum">
                <?= Yii::app()->cart->getCost(); ?>
            </div>
            <?php else: ?>
            <div class="but-cart__sum">
                нет товаров
            </div>
            <?php endif; ?>
        </div>
    <?php if (Yii::app()->cart->isEmpty()): ?>
        </a>
    <?php endif; ?>
</div>
