<div class="slider_wrapp">
<div class="slide-box slide-box-carousel">
        <?php $key = 1;?>
    <?php foreach ($models as $key => $slide): ?>
        <?php $key++;?>
        <div class="slide-box__item">
            <div class="container">
                <div class="slide-box__text">
                    <div class="slide-box__name">
                        <?= $slide->name; ?>
                    </div>
                    <div class="main-text">
                        <?= $slide->name_short; ?>
                    </div>
                    <div class="slide-desc">
                        <?= $slide->description_short; ?>
                    </div>
                   <div class="btns">
                    <div class="site_btn">
                      <a <?= $slide->button_link ?>><?= $slide->button_name ?></a>
                      <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/arr2.svg'); ?>
                    </div>
                    <div class="slide_btn-wrap">
                      <?= CHtml::link($slide->button_name2, $slide->button_link2,['class' => 'slide_btn']) ?>
                      <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/arr2.svg'); ?>
                    </div>
                   </div>
                </div>
            </div>
            <div class="slide-box__img">
                <img class="slide_md_img" src="" data-lazy="<?= $slide->getImageUrl(0,0,true,null,'image'); ?>" alt="">
                <img class="slide_xs_img" src="" data-lazy="<?= $slide->getImageXsUrl(0,0,true,null,'image_xs'); ?>" alt="">
            </div>

        </div>

    <?php endforeach ?>

    </div>
    <div class="container counter-container">
          <div class="counter">
            <span class="key-counter">0</span><span id="cp" class="key-counter">1</span> / <span class="item-count">0<?= $key;?></span>
           </div>
           <div class="counter_text">
            Классическая <br>строгость в сочетании<br> с яркими формами
           </div>
    </div>
 </div>

