 <div class="interer-box box2">
                <?php foreach ($model->images(['order' => 'position ASC']) as $key => $item): ?>

                  <div class="interer-box__item">
                        <a data-fancybox="item-<?= $model->id ?>" href="<?= $item->getImageUrl()?>" class="interer-box__item__img" style="position:relative;display:block;z-index:30">
                           <?= CHtml::image($item->getImageUrl()) ?>
                        </a>
                </div>
                <?php endforeach ?>

        </div>
        <?php $fancybox = $this->widget(
                    'gallery.extensions.fancybox3.AlFancybox', [
                        'target' => '[data-fancybox]',
                        'lang'   => 'ru',
                        'config' => [
                            'animationEffect' => "fade",
                            'buttons' => [
                                "zoom",
                                "close",
                            ]
                        ],
                    ]
                ); ?>