    <div class="container widget-container">
    <div class="widget-header">
        <h2 class="page_title">
            <?= $model->getCategoryName() ?>
        </h2> 

        <?php if (count($lists) < 5): ?>
        <?= CHtml::link('Все готовые решения', '/interery',['class' => 'btn-link nomargin']) ?>
            <?php else: ?>
        <?= CHtml::link('Все готовые решения','/interery',['class' => 'btn-link']) ?>
        <?php endif ?>
    </div> 

        <div class="solutuion-box">
                <?php foreach ($lists as $key => $item): ?>
                  <div class="solutuion-box__item">
                    <span class="solution-box__item__innerBox">
                        <span class="solutuion-box__item__img">
                            <img src="" data-src="<?= $item->previewImage(300,227,true); ?>" alt="">
                        </span>
                        <span class="solutuion-box__item__name"><?= $item->name?></span>
                    </span>
                    <?php foreach ($item->images as $k => $value): ?>
                        <?php if ($k == 0): ?>
                            <a href="<?= $value->getImageUrl()?>" class = "fimg" data-fancybox="<?= $item->id ?>"></a>
                            <?php else: ?>
                            <a href="<?= $value->getImageUrl()?>" style = "display: none" data-fancybox="<?= $item->id ?>"></a>
                        <?php endif ?>
                    <?php endforeach ?>

                </div>
                <?php endforeach ?>

        </div>

</div>
