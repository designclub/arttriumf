<?php if($models->images) : ?>
    <div class="partners-name">
        <div class="page_title">
            <?= $models->name; ?>
        </div>
        <div class="description">
            <?= $models->description; ?>
        </div>
    </div>
    <div class="partners-carousel-wrapper">
        <div class="partners-carousel">
            <?php foreach ($models->images as $key => $item): ?>
                <div class="partners__item">
                    <div class="partners__img">
                        <?= CHtml::image($item->getImageUrl(),$item->alt) ?>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
<?php endif; ?>