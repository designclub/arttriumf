<header>
    <div class="container d-flex">
        <a href="/" class="logo">
            <span class="logo__img">
                <?= CHtml::image($this->mainAssets . '/images/logo.png',Yii::app()->getModule('yupe')->siteName) ?>
            </span>
            <span class="logo__name">
                Салон интерьерных решений
            </span>
        </a>
        <div class="header__top">
            <div class="mail-main">
                <a href="mailto:<?= Yii::app()->getModule('yupe')->email?>"><?= Yii::app()->getModule('yupe')->email?></a>
                <a href="#" data-target = "#CallbackFormEmail" data-toggle = "modal" class="js-button">написать нам</a>
            </div>
            <div class="header_phone">

                <a href="" id="hide-phone" onclick="yaCounter56431171.reachGoal('closephone'); return false;" class="phone-main one">
                    +7 (902) ПОКАЗАТЬ
                </a>
                <a href="tel:<?= Yii::app()->getModule('yupe')->p_reception?>" id="return-phone" onclick="yaCounter56431171.reachGoal('zvonklik'); return true;" class="phone-main one hide"><?= Yii::app()->getModule('yupe')->reception?>
                </a>

                <a href="" id="hide-phone1" onclick="yaCounter56431171.reachGoal('closephone'); return false;" class="phone-main two">
                    +7 (349) ПОКАЗАТЬ
                </a>
                <a href="tel:<?= Yii::app()->getModule('yupe')->p_reception2?>" id="return-phone1" onclick="yaCounter56431171.reachGoal('zvonklik'); return true;" class="phone-main two hide"><?= Yii::app()->getModule('yupe')->reception2?>
                </a>
            </div>
            <span class="w-mode"><?= Yii::app()->getModule('yupe')->wmode?></span>
            <div class="favorits">
                <?php if(Yii::app()->hasModule('favorite')):?>
                    <a class="toolbar-button" href="<?= Yii::app()->createUrl('/favorite/default/index'); ?>">
                        <div class="but-favorite__icon but-header__icon">
                            <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/heart.svg'); ?>
                            <span class="but-favorite__count but-header__count <?= (Yii::app()->favorite->count() != null) ? 'active' : ''; ?>" id="yupe-store-favorite-total"><?= Yii::app()->favorite->count();?></span>
                        </div>
                        <div class="but-favorite__icon__text but-header__text">Избранное</div>
                    </a>
                <?php endif;?>
            </div>
            <div class="search">
                <a class="search-xs-link" href="#" data-toggle="modal" data-target="#searchModal">
                    <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/magnifer.svg'); ?>
                </a>
            </div>
            <div class="user">
                <?php if (Yii::app()->user->isGuest): ?>
                    <a class="" href="<?= Yii::app()->createUrl('user/account/login'); ?>">
                        <span class="icon">
                            <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/user.svg'); ?>
                        </span>
                    </a>
                <?php else: ?>
                    <a class="" href="<?= Yii::app()->createUrl('user/profile/index'); ?>">
                        <span class="icon">
                             <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/user.svg'); ?>
                        </span>
                    </a>
                <?php endif ?>
            </div>
            <div class="cart">
                <?php if (Yii::app()->hasModule('cart')): ?>
                    <div class="header-cart shopping-cart-widget" id="shopping-cart-widget">
                        <?php $this->widget('application.modules.cart.widgets.ShoppingCartWidget'); ?>
                    </div>
                <?php endif; ?>
            </div>
       </div>
    </div>
</header>
