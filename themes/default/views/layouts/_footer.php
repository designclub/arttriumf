<footer>
    <div class="container d-flex">
        <div class="footer_brand">
            <a href="/" class="footer__logo">
                <span class="footer__logo__img">
                    <?= CHtml::image($this->mainAssets . '/images/flogo.png') ?>
                </span>
                <span class="footer__logo__salon">
                   Салон интерьерных решений
                </span>
            </a>
            <div class="footer__copyright">
                © 2017-<?= date("Y"); ?> “Арт-Триумф”, салон <br>интерьерных решений. <br>Продажа отделочных материалов,<br> дизайн интерьера, декоративно- <br>отделочные работы
            </div>
        </div>
        <div class="footer__menu">
            <div class="footer__menu__title">
                Каталог товаров
            </div>
            <?php $this->widget('application.modules.store.widgets.CategoryWidget',['depth' => 0, 'view' => 'category-footer', 'limit' => 16]); ?>
        </div>

        <div class="footer__menu">
            <div class="footer__menu__title">
                Информация
            </div>
            <?php $this->widget('application.modules.menu.widgets.MenuWidget', ['name' => 'top-menu', 'view' => 'footer']); ?>
        </div>
        <div class="footer__menu contacts">
            <div class="footer__menu__title">
                Контакты
            </div>

            <a href="" id="hide-phone2" onclick="yaCounter56431171.reachGoal('closephone'); return false;" class="phone-footer">+7 (902) ПОКАЗАТЬ
            </a>
            <a href="tel:<?= Yii::app()->getModule('yupe')->p_reception?>" id="return-phone2" onclick="yaCounter56431171.reachGoal('zvonklik'); return true;" class="phone-footer hide"><?= Yii::app()->getModule('yupe')->reception?>
            </a>

            <span class="footer__w-mode"><?= Yii::app()->getModule('yupe')->wmode?></span>
            <a href="#" class="footer__call" data-target="#callbackModal" data-toggle="modal">Заказать звонок</a>
            <a href="mailto:<?= Yii::app()->getModule('yupe')->email?>" class = "footer-mail"><?= Yii::app()->getModule('yupe')->email?></a>
            <a href="#" class="footer__call" data-target="#CallbackFormEmail" data-toggle="modal">написать нам</a>
             <a href="https://vk.com/arttriumphsalekhard" class="social" target="_blank">
                <!-- /images/insta.svg -->
                <?= file_get_contents('.'.Yii::app()->getTheme()->getAssetsUrl(). '/images/svg/vk.svg') ?>
                <span>Мы в соц. сетях</span>
            </a>
        </div>
    </div>
    <div class="container">
        <div class="footer-botom">
            <div class="rights">Все права защищены</div>
            <a href="https://designclub56.ru/" class="dcmedia" target="_blank">
                <span class="dcmedia__img">
                </span>
                <span class="dcmedia__title">
                    Создание и продвижение сайтов
                </span>
            </a>
        </div>
    </div>
</footer>