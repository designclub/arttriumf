<!DOCTYPE html>
<html lang="<?= Yii::app()->language; ?>">
<head>
    <?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::HEAD_START);?>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Language" content="ru-RU" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <title><?= $this->title;?></title>
    <meta name="description" content="<?= $this->description;?>" />
    <meta name="keywords" content="<?= $this->keywords;?>" />
    <meta name="yandex-verification" content="1eb65c230b1e1861" />
    <!-- Яндекс метрика -->
<!--      Yii::app()->getClientScript()->registerScript(
        "yandex-metrika","
            function AppMetrica(){
                (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)}; m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)}) (window, document, \"script\", \"https://mc.yandex.ru/metrika/tag.js\", \"ym\"); ym(56431171, \"init\", { clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true });
            }
            $(document).one('scroll mosemove',AppMetrica);
            setTimeout(AppMetrica, 3000); 
        "
    ); 
    <noscript>
        <div><img src="https://mc.yandex.ru/watch/56431171" style="position:absolute; left:-9999px;" alt="" /></div>
    </noscript> -->
    
    <!-- Yandex.Metrika counter --> <script type="text/javascript" > (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)}; m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)}) (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym"); ym(56431171, "init", { clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); </script> <noscript><div><img src="https://mc.yandex.ru/watch/56431171" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->

    <?php if ($this->canonical): ?>
        <link rel="canonical" href="<?= $this->canonical; ?>" />
    <?php endif; ?>

    <script src="//code-ya.jivosite.com/widget/xIBT36Lsef" async></script>
    <?php
    Yii::app()->getClientScript()->registerCssFile($this->mainAssets . '/css/style.min.css');
    Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/store.js',CClientScript::POS_END);
    Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/main.min.js',CClientScript::POS_END);
    Yii::app()->getClientScript()->registerCoreScript('maskedinput');
    ?>
    <script type="text/javascript">
        var yupeTokenName = '<?= Yii::app()->getRequest()->csrfTokenName;?>';
        var yupeToken = '<?= Yii::app()->getRequest()->getCsrfToken();?>';
    </script>

    <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- <link rel="stylesheet" href="http://yandex.st/highlightjs/8.2/styles/github.min.css">
    <script src="http://yastatic.net/highlightjs/8.2/highlight.min.js"></script> -->

    <?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::HEAD_END);?>
    <meta name="yandex-verification" content="331ee693d83e4a78" />
    <meta name="google-site-verification" content="GrQ6U8uiclxCvAlGfi66L1Xg6CVFZ86NWZSAOc83xl4" />
    <!-- Pixel -->
    <script type="text/javascript">
        (function (d, w) {
            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script");
                s.type = "text/javascript";
                s.async = true;
                s.src = "https://qoopler.ru/index.php?ref="+d.referrer+"&page=" + encodeURIComponent(w.location.href);
                n.parentNode.insertBefore(s, n);
        })(document, window);
    </script>
<!-- /Pixel -->

</head>
<body>
    <?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::BODY_START);?>
    <div class="header_wrap">
        <?php $this->renderPartial('//layouts/_header'); ?>
        <?php $this->renderPartial('//layouts/_menu'); ?>
    </div>
    <div class='wrapper'>
        <div class="preloader" id="loading">
            <div id="circularG">
                <div id="circularG_1" class="circularG"></div>
                <div id="circularG_2" class="circularG"></div>
                <div id="circularG_3" class="circularG"></div>
                <div id="circularG_4" class="circularG"></div>
                <div id="circularG_5" class="circularG"></div>
                <div id="circularG_6" class="circularG"></div>
                <div id="circularG_7" class="circularG"></div>
                <div id="circularG_8" class="circularG"></div>
            </div>
        </div>

        <?php if (!$this->main_page): ?>
            <div class="container breadcrumbs_wrap">
                <?php $this->widget('bootstrap.widgets.TbBreadcrumbs', [
                    'links' => $this->breadcrumbs,
                ]); ?>
            </div>
        <?php endif ?>

        <?php $this->widget('yupe\widgets\YFlashMessages'); ?>

        <div class="content">
            <?= $this->decodeWidgets($content); ?>
        </div>

        <?php $this->renderPartial('//layouts/_footer'); ?>
    </div>

    <div class="modal fade" id="callback-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content"></div>
        </div>
    </div>

    <?php Yii::app()->clientScript->registerScript('callback-form', "
        $(document).delegate('.js-callback-modal', 'click', function() {
            $.get(this.href, function(data) {
                $('#callback-modal').find('.modal-content').html(data);
                $('#callback-modal').modal('show');
                jQuery('#CallbackForm_phone').mask('+7(999)999-99-99');
            })
            return false;
        });
    ") ?>

    <?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::BODY_END);?>


    <?php $this->widget('application.modules.review.widgets.ReviewWidget', ['view' => 'reviewmodalwidget']); ?>
    <?php $this->widget('application.modules.mail.widgets.CallbackFormEmailWidget'); ?>
    <?php $this->widget('application.modules.mail.widgets.CallbackWidget',['view' => 'callback-widget']); ?>
    <?php $this->widget('application.modules.store.widgets.SearchProductWidget', ['view' => 'search-product-form-modal']); ?>

    <?php $fancybox = $this->widget(
        'gallery.extensions.fancybox3.AlFancybox',
        [
            'target' => '[data-fancybox]',
            'lang'   => 'ru',
            'config' => [
                'animationEffect' => "fade",
                'buttons' => [
                    "zoom",
                    "close",
                ]
            ],
        ]
    ); ?>

    <div class='notifications top-right' id="notifications"></div>
    <div class="ajax-loading"></div>
</body>
</html>
