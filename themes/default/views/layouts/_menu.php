<div class="main-menu">
    <div class="container d-flex">
        <a class="catalog-link" href="#">
            <div class="toggler">
                <span></span>
            </div>

            <span class="catalog-limk__name">Каталог товаров</span>
            <span class="icon">
                <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/right.svg'); ?>
            </span>

        </a>
          <?php $this->widget('application.modules.menu.widgets.MenuWidget', ['name' => 'top-menu']); ?>

    </div>
<div class="container toggle-menu">
    <div class="close">x</div>
    <?php $this->widget('application.modules.store.widgets.CatalogWidget', ['view' => 'view-top']); ?>
    <?php $this->widget('application.modules.menu.widgets.MenuWidget', ['name' => 'top-menu', 'view' => 'toggle']); ?>
</div>
</div>