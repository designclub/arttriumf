<?php
/* @var $model Page */
/* @var $this PageController */

if ($model->layout) {
    $this->layout = "//layouts/{$model->layout}";
}

$this->title = $model->meta_title ?: $model->title;
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->meta_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->meta_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
$this->title_short = $model->title_short;

?>
<div class="container">
    <h1 class="page_title"><?= $model->title_short?></h1>
    <div class="about_wrapp">
    
	<div class="akcii">
		<div class="akcii-inner">
			<div class="akcii-item" style="background-image: 
									linear-gradient(to top, #202020, transparent 100%), 
									url(/uploads/akcii/akcii-item1.jpg); "> 
				<div class="akcii-item__head">
					Акция до 15 Апреля
				</div>
				<div class="akcii-item__foot">
					<span>Новоселам</span>
					<p>скидки до 15%</p>
				</div>
			</div>
			<div class="akcii-item" style="background-image: 
									linear-gradient(to top, #202020, transparent 100%), 
									url(/uploads/akcii/akcii-item2.jpg); "> 
				<div class="akcii-item__head">
					Акция до 15 Апреля
				</div>
				<div class="akcii-item__foot">
					<span>Молодым семьям</span>
					<p>скидки до 15%</p>
				</div>
			</div>
			<div class="akcii-item" style="background-image: 
									linear-gradient(to top, #202020, transparent 100%), 
									url(/uploads/akcii/akcii-item3.jpg); "> 
				<div class="akcii-item__foot-show">
					<ul>
						<li>Дизайн проект</li>
						<li>Подбор материалов из каталога</li>
						<li>Отделочные работы</li>
					</ul>
					<span>Скидка расчитывается индивидуально</span>
				</div>
				<div class="akcii-item__foot1">
					<span>Выгода комплексного</span>
					<p>обслуживания</p>
				</div>
			</div>
		</div>
	</div>

    </div>
</div>