<?php
/* @var $model Page */
/* @var $this PageController */

if ($model->layout) {
    $this->layout = "//layouts/{$model->layout}";
}

$this->title = $model->meta_title ?: $model->title;
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->meta_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->meta_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
$this->title_short = $model->title_short;

?>
<div class="container">
    <h1><?= $model->title; ?></h1>
        <div class="row">
            <div class="col-md-12">
                <main>
                     <div class="our_contacts" itemscope="" itemtype="http://schema.org/Organization">
                        <div class="adress"><p itemprop="name">Компания "Арт Триумф"</p></div>
                        <div class="adress" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
                            <div class="name">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                    Адрес
                            </div>
                            <div class="detail">
                                <div itemprop="addressLocality">город: Салехард</div>
                                <div itemprop="streetAddress">ул. Республики</div>
                                <div itemprop="streetAddress">дом: 77</div>
                            </div>
                        </div>
                        <div class="adress">
                            <div class="name">
                                    <i class="fa fa-phone-square" aria-hidden="true"></i>
                                    Телефон
                            </div>
                            <div class="detail">
                                <a href="tel:+79025161971" id="hide-phone3" itemprop="telephone" onclick="yaCounter56431171.reachGoal('closephone'); return false;">+7 (902) ПОКАЗАТЬ</a>
                                <a href="tel:+79025161971" itemprop="telephone" id="return-phone3" class="hide" onclick="yaCounter56431171.reachGoal('zvonklik'); return true;">+7 (902) 816 19 71</a>

                                <a href="tel:+73492261971" id="hide-phone4" itemprop="telephone" onclick="yaCounter56431171.reachGoal('closephone'); return false;">+7 (349) ПОКАЗАТЬ</a>
                                <a href="tel:+73492261971" itemprop="telephone" id="return-phone4" class="hide" onclick="yaCounter56431171.reachGoal('zvonklik'); return true;">+7 (349) 226 19 71</a>
                            </div>
                        </div>
                        <div class="adress">
                            <div class="name">
                                    <i class="fa fa-envelope" aria-hidden="true"></i>
                                    Email
                            </div>
                            <div class="detail">
                                <a href="mailto:artriumf@mail.ru" itemprop="email">artriumf@mail.ru</a>
                            </div>
                        </div>
                        <div class="adress">
                            <div class="name">
                                    <i class="fa fa-vk" aria-hidden="true"></i>
                                    ВКонтакте
                            </div>
                            <div class="detail">
                                <a href="https://vk.com/arttriumphsalekhard" itemprop="email">https://vk.com/arttriumphsalekhard</a>
                            </div>
                        </div>
                     </div>
                <div class="avantis_img">
                    <img src="/uploads/page/pppp.jpg" alt="">              </div>
            </main>
            <div class="contact">
                <h3 class="contact__title">Реквизиты</h3>
                <div class="contact__info">
                    <div class="contact__info-item">
                        <div class="contact-info__title">
                            Наименование
                        </div>
                        <div class="contact-info__desc">
                            ООО «Новый Рим»
                        </div>
                    </div>

                    <div class="contact__info-item">
                        <div class="contact-info__title">
                            ОГРН
                        </div>
                        <div class="contact-info__desc">
                            1184350008611
                        </div>
                    </div>

                    <div class="contact__info-item">
                        <div class="contact-info__title">
                            ИНН
                        </div>
                        <div class="contact-info__desc">
                            4345482144
                        </div>
                    </div>
                    <div class="contact__info-item">
                        <div class="contact-info__title">
                            КПП
                        </div>
                        <div class="contact-info__desc">
                            561001001
                        </div>
                    </div>
                    <div class="contact__info-item">
                        <div class="contact-info__title">
                            Юридический адрес:
                        </div>
                        <div class="contact-info__desc">
                           61002, Кировская обл.,г. Киров, ул. Блюхера,д.32, кв.108

                        </div>
                    </div>

                    <div class="contact__info-item">
                        <div class="contact-info__title">
                            Фактический адрес:
                        </div>
                        <div class="contact-info__desc">
                            629008, ЯНАО, г. Салехард, ул. Республики, д.77

                        </div>
                    </div>

                    <div class="contact__info-item">
                        <div class="contact-info__title">
                            Расчетный счет
                        </div>
                        <div class="contact-info__desc">
                            40702810167450000698
                        </div>
                    </div>
                    <div class="contact__info-item">
                        <div class="contact-info__title">
                            БИК
                        </div>
                        <div class="contact-info__desc">
                            047102651
                        </div>
                    </div>
                    <div class="contact__info-item">
                        <div class="contact-info__title">
                            Корреспондентсткий счет
                        </div>
                        <div class="contact-info__desc">
                            30101810800000000651
                        </div>
                    </div>
                </div>

            <div class="map_box">
            <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3A32c78e0e23d691347a32fc6f874d2f5ccf3b490afc44ac44c7dd43ef07d7232a&amp;source=constructor" width="100%" height="500" frameborder="0"></iframe>
            </div>
            </div>
        </div>
    </div>
