<?php
/* @var $model Page */
/* @var $this PageController */

if ($model->layout) {
    $this->layout = "//layouts/{$model->layout}";
}

$this->title = $model->meta_title ?: $model->title;
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->meta_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->meta_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
$this->title_short = $model->title_short;

?>
<section class="contacts-wrapper">
  <?php $this->widget('application.modules.contentblock.widgets.ContentBlockWidget',['code' => 'kontakty']); ?>
  <div class="contact-map">
  <div class="container">
    <div id="map" style="height:600px;"></div>
  </div>
</div>
</section>
    <script src="http://api-maps.yandex.ru/2.1/?lang=ru_RU"></script>
    <script>
        ymaps.ready(init);

function init () {
    var myMap = new ymaps.Map("map", {
            center: [55.15, 38.71],
            zoom: 6
        }),

    // Создаем геообъект с типом геометрии "Точка".
        myGeoObject = new ymaps.GeoObject();
    myMap.geoObjects
        .add(myGeoObject)
        .add(new ymaps.Placemark([55.808401, 38.988294], {
            balloonContent: 'ЗОВ <strong>г.Орехово-Зуево, ул.Ленина,  д.78</strong>'
        }, {
            iconLayout: 'default#image',
            iconImageHref: '<?= $this->mainAssets ."/images/map-icon02.png" ?>',
            iconImageSize: [40, 64],
            iconImageOffset: [-20, -64]
        }))
        .add(new ymaps.Placemark([57.004847, 40.930253], {
            balloonContent: 'ЗОВ <strong>г.Иваново, ул.Шевченко, д.19</strong>'
        }, {
            iconLayout: 'default#image',
            iconImageHref: '<?= $this->mainAssets ."/images/map-icon02.png" ?>',
            iconImageSize: [40, 64],
            iconImageOffset: [-20, -64]
        }))
        /*.add(new ymaps.Placemark([55.809564, 37.493990], {
            balloonContent: 'ЗОВ <strong>Волоколамское шоссе д.14</strong>'
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'assets/templates/zov33/images/map-icon02.png',
            iconImageSize: [40, 64],
            iconImageOffset: [-20, -64]
        }))*/;
}
    </script>


