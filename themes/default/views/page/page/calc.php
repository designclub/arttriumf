<?php
/* @var $model Page */
/* @var $this PageController */

if ($model->layout) {
    $this->layout = "//layouts/{$model->layout}";
}

$this->title = $model->meta_title ?: $model->title;
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->meta_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->meta_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
$this->title_short = $model->title_short;

?>
<div class="calc-page">
    <div class="container">
        <div class="qweeqly">
            <span>Быстро и совершенно бесплатно </span><br>сделаем дизайн-проект вашей кухни и рассчитаем стоимость кухонного гарнитура
        </div>
      <?php $this->widget('application.modules.mail.widgets.CalcWidget'); ?>
    </div>
</div>

