
      <div class="product-section">
            <span class="page_title">Наши работы</span>
            <a href="/nashi-raboty" class="btn-link nomargin">
              Все работы
            </a>
      </div>
<div class="srvices-carousel">
      <?php foreach ($pages as $key => $page) : ?>
        <div class="services__item-wrap">
            <div class="services__item">
                <div class="services__item-img">
                  <?= CHtml::image($page->getImageUrl(0,0,true,null,"icon")) ?>
                </div>
                <div class="services__item-title">
                  <?= $page->title_short ?>
                </div>
                <div class="services__item-desc">
                  <?= $page->year?>
                </div>
            </div>
        </div>
      <?php endforeach;?>
 </div>




