<div class="container widget-container">
    <div class="widget-header">
        <h2 class="page_title">
            <?= $pages->title ?>
        </h2>
        <?php if (count($pages->childPages) < 5): ?>
        <?= CHtml::link('Все готовые решения', ['/page/page/view', 'slug' => $pages->slug],['class' => 'btn-link nomargin']) ?>
            <?php else: ?>
        <?= CHtml::link('Все готовые решения', ['/page/page/view', 'slug' => $pages->slug],['class' => 'btn-link']) ?>
        <?php endif ?>
    </div>
    <?php if (!empty($pages->childPages)): ?>
        <div class="solutuion-box">
            <?php foreach ($pages->childPages as $key => $value): ?>
                <a class="solutuion-box__item" href="<?= Yii::app()->createUrl('/page/page/view', ['slug'=>$value->slug]) ?>">
                    <span class="solution-box__item__innerBox">
                        <span class="solutuion-box__item__img">
                           <?= CHtml::image($value->getImageUrl(300,227,true,null,"icon")) ?>
                        </span>
                        <span class="solutuion-box__item__name"><?= $value->title_short?></span>
                    </span>
                </a>
            <?php endforeach ?>
        </div>
    <?php endif ?>
</div>
