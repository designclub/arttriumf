<h2 class="title text-center">
    <span>
        <?= $pages->title_short; ?>
    </span>
</h2>
<div class="company-home__info">
    <div class="company-home__right">
        <div class="company-home__right-text">
             <?= $pages->body_short; ?>
        </div>

            <a class="but but-yellow" href="<?= $pages->getUrl(); ?>">Подробнее</a>

    </div>
    <div class="company-home__left">
        <div class="company-home__left-img">
        <?= CHtml::image($pages->getImageUrl(), $pages->title_short, ['class' => 'img-sm']); ?>
         <?= CHtml::image($pages->getIconUrl(), $pages->title_short, ['class' => 'img-xs']); ?>
        </div>
    </div>
</div>