<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
    'id' => 'calculator-form',
    'type' => 'vertical',
]) ?>
<div class="parameters" id="to_print">

   <div class="material">
    <h2 class="calc_head"><span>Выбор фактуры</span></h2>
     <div class="inputs">
            <?= $form->textFieldGroup($model, 'area',[
                'widgetOptions' => [
                    'htmlOptions' => [
                        'placeholder' => 'м2',
                    ],
                ]
            ]) ?>

            <?= $form->dropDownListGroup($model, 'material', [
                'widgetOptions' => [
                    'data' => $model->getMaterialList(),
                    'htmlOptions' => [
                        'empty' => '---',
                    ],
                ]
            ]) ?>
            <?= $form->dropDownListGroup($model, 'color', [
                'widgetOptions' => [
                    'data' => $model->getColorList(),
                    'htmlOptions' => [
                        'empty' => '---',
                    ],
                ]
            ]) ?>
            <?= $form->dropDownListGroup($model, 'texture', [
                'widgetOptions' => [
                    'data' => $model->getTextureList(),
                    'htmlOptions' => [
                        'empty' => '---',
                    ],
                ]
            ]) ?>
    </div>
</div>
<div class="dops">
    <h2 class="calc_head"><span>Дополнительно</span></h2>
    <div class="inputs">
            <?= $form->textFieldGroup($model, 'angles', [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'placeholder' => 'шт.',
                        'class' => '',
                    ],
                ]
            ]) ?>
            <?= $form->textFieldGroup($model, 'lamps', [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'placeholder' => 'шт.',
                        'class' => '',
                    ],
                ]
            ]) ?>
            <?= $form->textFieldGroup($model, 'pipe', [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'placeholder' => 'шт.',
                        'class' => '',
                    ],
                ]
            ]) ?>
            <?= $form->textFieldGroup($model, 'curtains', [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'placeholder' => 'м.п',
                        'class' => '',
                    ],
                ]
            ]) ?>
            <?= $form->textFieldGroup($model, 'niche_open',  [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'placeholder' => 'м.п',
                        'class' => '',
                    ],
                ]
            ]) ?>
            <?= $form->textFieldGroup($model, 'niche_closed',  [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'placeholder' => 'м.п',
                        'class' => '',
                    ],
                ]
            ]) ?>
     </div>
   </div>
</div>
   <div class="resultDiv">
        <?php if ($model->getCost()) : ?>
            <div class="alert">
                <div class="price_res">Итого: <?= $model->getCost() ?>руб.</div class="price_res">
                    <div class="print_btn">
                        <label><span>Нажмите</span>
                        <button class="btn btn-default" id="print"></button>
                        <span>чтобы распечатать</span>
                        </label>
                    </div>
            </div>
        <?php endif ?>
  </div>
<?php $this->endWidget();

Yii::app()->clientScript->registerScript('calculator', "
$(document).delegate('#calculator-form', 'change', function() {
    var form = $(this),
    url = form.attr('action'),
    type = form.attr('method'),
    data = form.serialize(),
    selector = '#'+form.attr('id');

    $.ajax({
        url: url,
        type: type,
        data: data,
        success: function(data) {
            var html = $(data).find(selector).html();
            $(selector).html(html);
        }
    });
});
");