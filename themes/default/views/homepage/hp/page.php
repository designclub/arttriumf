<?php
/** @var Page $page */

if ($page->layout) { 
    $this->layout = "//layouts/{$page->layout}";
}
$this->main_page = true;
$this->title = !empty($page->meta_title) ? $page->meta_title : Yii::app()->getModule('yupe')->siteName;
$this->description = !empty($page->meta_description) ? $page->meta_description : Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = !empty($page->meta_keywords) ? $page->meta_keywords : Yii::app()->getModule('yupe')->siteKeyWords;
?> 
<?php $this->widget('application.modules.slider.widgets.SliderWidget'); ?>
<div class="container">
  <?= $page->body; ?>
</div>
<?php $this->widget('application.modules.gallery.widgets.GalleryNewWidget',['id' => 29, 'view' => 'gallery-widget', 'limit' => 6, 'category_id' => 1]); ?>
<section class="catalog-section">
    <div class="container">
        <div class="widget-header">
        <h2 class="page_title"> 
            Каталог товаров
        </h2>
        <?= CHtml::link('Перейти в каталог', '/store',['class' => 'btn-link nomargin']) ?>
    </div>
    <?php $this->widget('application.modules.store.widgets.CategoryWidget',['view' => 'view-home']); ?>
      <a href="/store" class="store-link">Перейти в каталог</a>
    </div>
</section>
<?php $this->renderPartial('//layouts/_bests'); ?>
<section class="works">
  <div class="container">
    <?php $this->widget('application.modules.page.widgets.PagesNewWidget',['view' => 'works', 'id' => 29]); ?>
  </div>
</section>
<section class="main">
  <div class="container d-flex">
    <div class="main__left">
      <h2 class="page_title">
        <?= $page->title_short?>
      </h2>
      <span class="art-triumf">
        <?= $page->year?>
      </span>
      <div class="main__left__txt">
        <?= $page->short_content?>
      </div>
      <div class="main__left__img">
        <?= CHtml::image($page->getImageUrl(549,471,true,null,"image")) ?>
      </div>
    </div>
    <div class="main__right">
      <div class="main__right__images">
        <div class="images__left">
          <?= CHtml::image($page->getImageUrl(219,454,true,null,"icon")) ?>
        </div>
        <div class="images__right">
          <?= CHtml::image($page->getImageUrl(409,499,true,null,"svg")) ?>
        </div>
      </div>
      <div class="page_title">
        <?= $page->under_title?>
      </div>
      <div class="main__right__txt">
        <?= $page->bonus?>
      </div>
      <div class="main__right__btns">
          <a href="#" class="site_btn" href="#" data-target = "#CallbackFormEmail" data-toggle = "modal">
            <span>Задать вопрос</span>
            <?= file_get_contents('.'.Yii::app()->getTheme()->getAssetsUrl(). '/images/arr3.svg') ?>
          </a>
<!--           <a href="#" data-target = "#CallbackFormEmail" data-toggle = "modal" class="js-button ques">
            Задать вопрос
          </a> -->
      </div>
    </div>
  </div>
</section>

