    <script src="http://api-maps.yandex.ru/2.1/?lang=ru_RU"></script>
    <script>
        ymaps.ready(init);

function init () {
    var myMap = new ymaps.Map("map", {
            center: [55.15, 38.71],
            zoom: 6
        }),

    // Создаем геообъект с типом геометрии "Точка".
        myGeoObject = new ymaps.GeoObject();
    myMap.geoObjects
        .add(myGeoObject)
        .add(new ymaps.Placemark([55.808401, 38.988294], {
            balloonContent: 'ЗОВ <strong>г.Орехово-Зуево, ул.Ленина,  д.78</strong>'
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'assets/templates/zov33/images/map-icon02.png',
            iconImageSize: [40, 64],
            iconImageOffset: [-20, -64]
        }))
        .add(new ymaps.Placemark([57.004847, 40.930253], {
            balloonContent: 'ЗОВ <strong>г.Иваново, ул.Шевченко, д.19</strong>'
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'assets/templates/zov33/images/map-icon02.png',
            iconImageSize: [40, 64],
            iconImageOffset: [-20, -64]
        }))
        /*.add(new ymaps.Placemark([55.809564, 37.493990], {
            balloonContent: 'ЗОВ <strong>Волоколамское шоссе д.14</strong>'
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'assets/templates/zov33/images/map-icon02.png',
            iconImageSize: [40, 64],
            iconImageOffset: [-20, -64]
        }))*/;
}
    </script>

<div id="map" style="height:600px; margin-bottom:25px;" class="container_15">

</div>